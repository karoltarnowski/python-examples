# -*- coding: utf-8 -*-
"""
Podstawowe właściwości klasy ndarray
"""

import numpy as np

# utworzenie wektora o trzech elementach
a = np.array([1,2,3])
print("type(a)", type(a))
print("a =", a)

# pole shape - krotka zawierająca rozmiar tablicy
print("a.shape =", a.shape)

b = np.array([[1, 2, 3], [4, 5, 6]])
print("b =", b)
print("b.shape =", b.shape)

# pole size - liczba elementów tablicy
print("a.size =", a.size)
print("b.size =", b.size)

# pole ndim - liczba wymiarów
print("a.ndim =", a.ndim)
print("b.ndim =", b.ndim)

# pojedyncza liczba jest zamieniana
# w tablicę zerowymiarową
c = np.array(7)
print("c =", c)
print("c.shape =", c.shape)
print("c.size =", c.size)
print("c.ndim =", c.ndim)

















