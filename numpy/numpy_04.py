# -*- coding: utf-8 -*-
"""
Tworzenie obiektów ndarray
przy pomocy funkcji ones, diag, eye.
"""

import numpy as np

# tworzenie wektora jedynek
a = np.ones(shape = (3))
print("a =", a)

# utworzenie macierzy diagnonalnej
b = np.diag(a)
print("b =", b)

# tworzenie macierzy jednostkowej
c = np.eye(3)
print("c =", c)











