# -*- coding: utf-8 -*-
"""
Tworzenie ndarray z funkcji
funkcją fromfunction.
"""

import numpy as np

#tablica jednowymiarowa
fun1d = lambda x : x
a = np.fromfunction(fun1d, (15, ))
print(a)

#tablica dwuwymiarowa (tabliczka mnożenia)
fun2d = lambda x, y : x*y
b = np.fromfunction(fun2d, (10, 10), dtype = int)
print(b)

