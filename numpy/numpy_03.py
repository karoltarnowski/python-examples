# -*- coding: utf-8 -*-
"""
Tworzenie obiektów ndarray
funkcjami array, zeros, ones.
"""

import numpy as np

# tworzenie tablicy z krotek
a = np.array(((1,2),(3,4)))
print("a =", a)

# tworzenie tablicy zer
b = np.zeros(shape=(2,2))
print("b =", b)

# tablica trójwymiarowa
c = np.zeros(shape=(2,2,2))
print("c =", c)
print("c.shape =", c.shape)
print("c.ndim =", c.ndim)
print("c.size =", c.size)

# tablica jedynek
d = np.ones(shape = (10,))
print("d =", d)
print("d.shape =", d.shape)
































