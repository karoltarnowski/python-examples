# -*- coding: utf-8 -*-
"""
Tworzenie obiektów ndarray różnych typów.
"""

import numpy as np

# tablica typu int32
# (na podstawie typów danych wejściowych)
a = np.array((1,2,3))
print("a =", a)
print("a.dtype =", a.dtype)

# tablica typu float64
# (na podstawie typów danych wejściowych)
b = np.array((1,2.,3))
print("b =", b)
print("b.dtype =", b.dtype)

# tablica typu float64 (typ domyślny)
c = np.zeros(shape = 3)
print("c =", c)
print("c.dtype =", c.dtype)

# tablica typu int32 (typ przekazany jako argument)
d = np.zeros(shape = 3, dtype = "int32")
print("d =", d)
print("d.dtype =", d.dtype)

# tablica typu int8
# (liczba 255 nie mieści się w zakresie typu)
e = np.array([1, -1, 255], dtype = "int8")
print("e =", e)
print("e.dtype =", e.dtype)

# tablica typu uint8 
# (typ unisigned int nie przechowuje znaku)
f = np.array([1, -1, 255], dtype = "uint8")
print("f =", f)
print("f.dtype =", f.dtype)

#tablica typu complex128
g = np.array([1+1j])
print("g =", g)
print("g.dtype =", g.dtype)




