# -*- coding: utf-8 -*-
"""
Indeksowanie elementów.
"""

import numpy as np

#wektor liczb losowych
a = np.arange(0,10)
print(a)

#indeksowanie listą
index_list = [1, 3, 9]
print(a[index_list])

#indeksowanie tablicą
index_array = np.array([0,2,8])
print(a[index_array])

#indeksowanie tablicą logiczną
condition = a % 2 == 0
print(condition)
print(a[condition])
print(a[a % 2 == 0])





