"""
Zapisywanie danych do plików.
"""

import numpy as np

# przygotowanie danych testowych
a = np.random.rand(20,40)
b = np.random.rand(40,20)
c = a@b

# kontrolne wyświetlenie danych
print(a)
print(b)
print(c)

# zapis danych do plików tekstowych
np.savetxt("data_a.txt", a)
np.savetxt("data_b.txt", b)

# zapis danych do plików binarnych npy
np.save("data_a.npy", a)
np.save("data_b.npy", b)

# zapis danych do plików binarnych npz
np.savez("data.npz", a, b)
np.savez("data_kw.npz", a=a, b=b)











