# -*- coding: utf-8 -*-
"""
Tworzenie obiektów ndarray funkcjami arange, linspace.
"""

import numpy as np

# tworzenie wektora funkcją arange
a = np.arange(1,2,.1)
print("a =", a)

# tworzenie wektora funkcją linspace
b = np.linspace(1, 2, 11)
print("b =", b)















