# -*- coding: utf-8 -*-
"""
Prosty przykład użycia bilioteki numpy
"""

# importowanie bilioteki numpy
import numpy as np

# "dodawanie" list jest ich konkatenacją
a = [1, 2, 3]
b = [2, 3, 4]
print(a + b)

# funkcja array tworzy tablicę danych
npa = np.array(a)
npb = np.array(b)

# biblioteka numpy zawiera definicję klasy ndarray
# reprezentującej tablice danych
print(type(npa))

# tablice ndarray można np. dodawać element po elemencie
npc = npa + npb
print(npc)

