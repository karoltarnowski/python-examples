# -*- coding: utf-8 -*-
"""
Obliczanie wartości funkcji
od elementów tablicy.
"""

import numpy as np

a = np.linspace(0, 2*np.pi, 11)
cosa = np.cos(a)
print("a =", a)
print("cos(a) =", cosa)

