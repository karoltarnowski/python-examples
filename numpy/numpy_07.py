# -*- coding: utf-8 -*-
"""
Indeksowanie tablic ndarray.
"""

import numpy as np

#tablica rozmiaru 3x3 
a = np.arange(1,10,1).reshape(3,3)
print("a =", a)

# indeksowanie elementów tablicy od zera
# element 1, 1
print("a[1,1] =", a[1,1])
print("a[1][1] =", a[1][1])
# zerowy wiersz
print("a[0] =", a[0])
# cała tablica
print("a[:] =", a[:])

