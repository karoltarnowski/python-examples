# -*- coding: utf-8 -*-
"""
Funkcja jako argument innej funkcji
"""

def shout(text):
    return text.upper()

def whisper(text):
    return text.lower()

def greet(func):
    greeting = func("Dzień dobry!")
    print(greeting)

greet(shout)
greet(whisper)
