# -*- coding: utf-8 -*-
"""
"""

"""
@decorator
def hello_decorator():
    print("Cokolwiek")

"""
"""
Równoważny zapis bez dekoratora.
"""  
def hello_decorator():
    print("Cokolwiek")
hello_decorator = decorator(hello_decorator)

