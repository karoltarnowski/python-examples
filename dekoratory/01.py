# -*- coding: utf-8 -*-
"""
"""

"""
class C:
    def meth():
        pass
    
    meth = staticmethod(meth)
"""   

class C:
    @staticmethod
    def meth():
        pass
    