# -*- coding: utf-8 -*-
"""
"""

def ubierz(choinka):
    def ubrana_choinka():
        print("*")
        choinka()
        print("x")
    
    return ubrana_choinka

@ubierz
def choinka():
    print("choinka")
    

choinka()
"""
print()
choinka = ubierz(choinka)
choinka()
"""