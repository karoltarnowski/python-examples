#Program pokazuje formatowanie liczby zmiennoprzecinkowej
#przy użyciu funkcji format().

def main():
    kredyt = 5000.
    rata_miesieczna = kredyt / 12
    #liczba zmiennoprzecinkowa (f) jest zaokrąglona
    #do dwóch (.2f) cyfr po przecinku
    print('Miesięczna rata wynosi',
          format(rata_miesieczna, '.2f'),
          'zł.')

main()










