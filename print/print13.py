#Program pokazuje przykłady wykorzystania
#metody str.format() do formatowania napisów

def main():
    # dostęp do argumentów według pozycji
    print('{0}, {1}, {2}'.format('a', 'b', 'c'))
    print('{}, {}, {}'.format('a', 'b', 'c')) 
    print('{2}, {1}, {0}'.format('a', 'b', 'c'))
    print('{0}{1}{0}'.format('abra', 'cad'))   # indeksy argumentów mogą się powtarzać
    input()

    # dostęp do argumentów po nazwach
    print('Coordinates: {latitude}, {longitude}'.format(latitude='37.24N', longitude='-115.81W'))
    input()

    # wyrównywanie i ustalanie szerokości pola
    print('{:<30}'.format('wyrównanie do lewej'))
    print('{:>30}'.format('wyrównanie do prawej'))
    print('{:^30}'.format('wyśrodkowanie'))
    print('{:*^30}'.format('wyśrodkowanie'))   # i wypełnienie '*'
    input()

    # pokazywanie znaku liczb dodatnich
    print('{:+f}; {:+f}'.format(3.14, -3.14))  # pokazuj znak zawsze
    print('{: f}; {: f}'.format(3.14, -3.14))  # spacja dla dodatnich
    print('{:-f}; {:-f}'.format(3.14, -3.14))  # pokazuj tylko dla ujemnych
    print('{:f}; {:f}'.format(3.14, -3.14))  # pokazuj tylko dla ujemnych
    input()

    # liczby całkowite w innych systemach
    print("int: {0:d};  hex: {0:x};  oct: {0:o};  bin: {0:b}".format(42))
    print("int: {0:d};  hex: {0:#x};  oct: {0:#o};  bin: {0:#b}".format(42)) #z prefixami
    input()
    
    #wartości procentowe
    print('Correct answers: {:.2%}'.format(.5))


main()


















