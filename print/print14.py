#Program pokazuje przykład wykorzystania f-stringów

def main():
    latitude='37.24N'
    longitude='-115.81W'
    print(f'Coordinates: {latitude}, {longitude}')

    x = 3.
    y = 4.
    print(f'P({x: 7.2f}, {y: 7.2f})')

main()


















