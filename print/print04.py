#Program pokazuje jak zmienić domyślne zachowanie funkcji print()
#Napisy będą wyświetlone w tym samym wierszu, rozdzielone spacjami
def main():
    print('Raz', end=' ') #po wypisaniu napisu kursor nie jest przenoszony
    print('Dwa', end=' ') #do nowego wiersza, jest wstawiana spacja
    print('Trzy')

main()
