#Program pokazuje konkatencję ciągów tekstowych.

def main():
    #Dwa napisy zostaną połączone w jeden.
    print('To jest ' + 'jeden ciąg tekstowy.')

    #Trzy napisy zostaną połączone, mimo tego,
    #że znajdują się w kilku liniach.
    print('To jest bardzo długi napis, ' +
          'który trudno byłoby zmieścić ' +
          'w jednej linii.')

main()





