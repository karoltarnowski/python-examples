#Program pokazuje formatowanie
#liczby zmiennoprzecinkowej - użycie f-stringa

def main():
    kredyt = 5000.
    rata_miesieczna = kredyt / 12
    #liczba zmiennoprzecinkowa (f) jest zaokrąglona
    #do dwóch (.2f) cyfr po przecinku
    print(f'Miesięczna rata wynosi {rata_miesieczna: .2f} zł.')

main()










