#Program pokazuje jak zmienić domyślne zachowanie funkcji print()
# wywoływanej z kilkoma argumentami.
def main():
    print('Raz','Dwa','Trzy',sep='***') #napisy są rozdzielone gwiazdkami

main()
