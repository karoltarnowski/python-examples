#Program pokazuje jak zmienić domyślne zachowanie funkcji print()
#Napisy będą wyświetlone w tym samym wierszu.
def main():
    print('Raz', end='') #po wypisaniu napisu kursor pozostaje w miejscu
    print('Dwa', end='') #kolejne wywołanie wypisuje następny napis
    print('Trzy')

main()
