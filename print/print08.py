#Program pokazuje wykorzystanie znaków sterujących \n, \t, \', \", \\
def main():
    print('To jest napis') #Funkcja print() wyświetli napis
    
    #Znak sterujący \n powoduje przeniesienie kursora do nowej linii
    print('To\njest\nnapis')

    #Znak sterujący \t powoduje przejście do następnego położenia tabulatora
    print('To\tjest\tnapis')
    #Może to być pomocne przy formatowaniu nagłówka tabeli
    print('pn\twt\tśr\tcz\tpt')

    #Znaki specjalne \' oraz \" pozwalają uzyskać apostrof i cudzysłów
    print('Apostrof: \' ')
    print('Cudzysłów: \" ')

    #Skoro znaki sterujące wykorzystują ukośnik
    #To jak uzyskać ukośnik?
    print('Ukośnik: \\')

main()
