#Program pokazuje efekt wyświetlenia kilku liczb
#zmiennoprzecinkowych w kolumnie według wspólnego formatu.
#Liczby są wyrównane względem przecinka dziesiętnego.

def main():
    num1 = 127.899
    num2 = 3465.148
    num3 = 3.776
    num4 = 264.821
    num5 = 88.081
    num6 = 799.999

    form = '7.2f'

    print(format(num1,form))
    print(format(num2,form))
    print(format(num3,form))
    print(format(num4,form))
    print(format(num5,form))
    print(format(num6,form))

main()










