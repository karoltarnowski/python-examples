def roznica(odjemna, odjemnik):
    return odjemna - odjemnik

# argumenty przekazane jako pozycyjne
print(roznica(5,8))  
# argumenty przekazane jako pozycyjne
print(roznica(8,5))  

# pierwszy argumenty przekazany jako pozycyjny
# drugi jako argument nazwany
print(roznica(13, odjemnik = 8))
# argumenty przekazane jako nazwane
print(roznica(odjemna = 21, odjemnik = 13))
print(roznica(odjemnik = 21, odjemna = 44))



