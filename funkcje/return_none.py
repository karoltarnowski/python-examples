"""
Przykład pokazuje wykorzystanie typu NoneType,
gdy funkcja "nic nie zwraca".
"""

def fun():
    print("Komunikat z funkcji fun()")
    
def fun2():
    print("Komunikat z funkcji fun2()")
    return

print(fun())
print(fun2())


