#Program pokazuje, użycie dwóch zmiennych lokalnych
#o tych samych nazwach, w różnych funkcjach

def main():
    dolnoslaskie()
    mazowieckie()

#Funkcja dolnoslaskie() tworzy lokalną zmienną population.
def dolnoslaskie():
    population = 2902365
    print('Liczba mieszkańców województwa dolnośląskiego to ',\
          population,'.')

#Funkcja mazowieckie tworzy lokalną zmienną population.
def mazowieckie():
    population = 5391813
    print('Liczba mieszkańców województwa mazowieckiego to ',\
          population,'.')

main()







