#Program pokazuje przekazanie argumentu funkcji.

def main():
    value = 13 #przypisanie zmiennej value wartości 13
    #wywołanie funkcji show_double() z przekazaniem do niej argumentu
    show_double(value) 

#Funkcja show_double() pobiera argument
#i wyświetla jego podwojoną wartość
def show_double(number):
    result = number * 2
    print(result)

main()







