#Program pokazuje definicję i wywołanie funkcji zwracajacej wartość.

#Program pyta użytkownika o liczbę jabłek i pomarańczy.
#Następnie wywołuje funkcję sum() do obliczenia łącznej liczby owoców
#i wyświetla stosowny komunikat.
def main():
    apples  = int(input('Ile masz jabłek? '))
    oranges = int(input('Ile masz pomarańczy? '))
    print('Liczba wszystkich owoców',sum(apples,oranges))

#Funkcja sum oblicza sumę dwóch liczb
def sum(num1, num2):
    result = num1 + num2 #zapisanie wyniku w zmiennej pomocniczej
    return result        #zwrócenie wyniku

main()




