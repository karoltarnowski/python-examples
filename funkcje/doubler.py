"""
Przykład pokazuje definicję prostej funkcji i jej wywołania
z argumentami różnych typów
"""

def doubler(x):
    return 2*x
    
print(doubler(3))
print(doubler(3.))
print(doubler("3"))


