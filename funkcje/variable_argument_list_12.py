# Program pokazuje funkcję przyjmującą:
# wymagane argumenty (a, b, c),
# różną liczbę arguemntów pozycyjnych
# różną liczbę argumentów nazwanych.
def fun(a, b, c, *args, **kwargs):
    print(a)
    print(b)
    print(c)

    for item in args:
        print(item)

    for key, value in kwargs.items():
        print(key, value, sep=':')

fun('1', '2', '3', '4', '5', k1 = 'v1', k2 = 'v2', k3 = 'v3')
fun(a = '1', b = '2', c = '3', d = '77', d = '55')




