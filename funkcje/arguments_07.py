# -*- coding: utf-8 -*-
"""
Definicja funkcji przyjmującej wiele argumentów
(operator *)
"""

def f(*a):
    for x in a:
        print(x)
    
    
f(1)

print()

f(2,3,4,5)



