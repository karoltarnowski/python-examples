#Program pokazuje przekazanie dwóch argumentów do funkcji.

def main():
    print('Suma liczb 12 i 45 wynosi')
    #Wartości wyrażeń użytych w miejscu wywołania
    #zostaną nadane zmiennym w funkcji show_sum().
    show_sum(12,45)
    
#Funkcja show_sum() pobiera dwa argumenty
#i wyświetla ich sumę.
#W nawiasie umieszczono listę argumentów.
#Nazwy argumentów są rozdzielone przecinkiem.
def show_sum(num1, num2):
    result = num1 + num2
    print(result)

main()







