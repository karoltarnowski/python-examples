#Ten program pokazuje przykład wywołania funkcji
#z użyciem argumentów nazwanych oraz pozycyjnych.

#Funkcja główna wywołuje funkcję wyświetlającą obliczony wynik.
def main():
    #W tym wywołaniu argumenty przyjmują wartości na podstawie pozycji.
    show_interest(1000,0.03,3)

    #W tym wywołaniu argumenty przyjmują wartości zgodnie z nazwą.
    #Kolejność nie ma znaczenia.
    show_interest(deposit = 1000, rate = 0.03, periods = 3)
    show_interest(rate = 0.03, periods = 3, deposit = 1000)

    #W tym wywołaniu pierwszy argument przyjmuje wartość dzięki pozycji.
    #Kolejne na podstawie nazwy.
    show_interest(1000, periods = 3, rate = 0.03)

#Funkcja wyświetlająca wysokość odsetek
#należnych od zdeponowanej kwoty
#w zależności od czasu trwania depozytu
#oraz wysokości oprocentowania.
def show_interest(deposit, rate, periods):
    interest = deposit * rate * periods
    print('Wysokość odsetek wynosi',
          format(interest, '.2f')
          )

#Wywołanie funkcji głównej
main()



