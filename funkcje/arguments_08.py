# -*- coding: utf-8 -*-
"""
Definicja funkcji przyjmującej wiele argumentów nazwanych
(operator **)
"""

def f(**a):
    for k, v in a.items():
        print(k, v)
    
    
f(a=1)

f(b=2,c=3,d=4,e=5)



