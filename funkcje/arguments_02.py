# -*- coding: utf-8 -*-
"""
Przykład ilustruje przekazanie argumentów
przez wartość oraz przez referencję
"""

def f(a, b):
    a = 2
    b[0] = 'python'
    
x = 1
l = [1, 2]
f(x,l)
print(x, l)



