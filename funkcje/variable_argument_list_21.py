# Program pokazuje funkcję przyjmującą co najmniej 2 argumenty pozycyjne.
def fun(a, b, *args):
    print(a)
    print(b)
    print(args)

fun('a', 'b', 'c')
fun('a', 'b', 'c', 'd')
#fun('a')
#fun()





