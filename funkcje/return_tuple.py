#Przykładowy program wywołujący funkcję zwracającą kilka wartości.

#Funkcja główna programu
def main():
    first_name, last_name = get_name()  #wczytanie imienia i nazwiska
    #wyświetlenie powitania (operator + użyty do połączenia łańcuchów)
    print('Witaj,', first_name + ' ' + last_name )

#Funkcja get_name()
#  wejście:       brak
#  przetwarzanie: funkcja prosi użytkownika o podanie imienia
#                 funkcja prosi użytkownika o podanie nazwiska
#  wyjście:       imię i nazwisko (jako dwa łańcuchy znakowe)
def get_name():
    first = input('Podaj imię: ')
    last  = input('Podaj nazwisko: ')
    return first, last

main()



