#Przykładowy program wywołujący funkcję zwracającą łańcuch tekstowy

#Funkcja główna programu
def main():
    name = get_name()       #wczytanie imienia
    print('Witaj,',name)    #wyświetlenie powitania   

#Funkcja get_name()
#  wejście:       brak
#  przetwarzanie: funkcja prosi użytkownika o podanie imienia
#  wyjście:       imię (jako łańcuch znakowy)
def get_name():
    name = input('Podaj imię: ')
    return name

main()



