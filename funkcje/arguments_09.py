# -*- coding: utf-8 -*-
"""
Definicja funkcji, w której opcjonalny argument 
ma domyślną wartość None
"""

def f(a, b = None):
    if b is None:
        print('Wywołanie z jednym argumentem', a)
    else:
        print('Wywołanie z dwoma argumentami', a, b)
        
    
    
f(1)
f(2,3)



