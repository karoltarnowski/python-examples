def main():
    print('2 * 3 =',my_product1(2,3))
    print('2 * 3 =',my_product2([2,3]))
    print('2 * 3 * 4 =',my_product2([2,3,4]))
    print('2 * 3 * 4 =',my_product2(2,3,4))
    # podano więcej niż jeden argument
   
def my_product1(a,b):
    return a*b

def my_product2(l):
    result = 1
    for x in l:
        result *= x
    return result

main()

#2 * 3 = 6
#2 * 3 = 6
#2 * 3 * 4 = 24
#Traceback (most recent call last):
#  File "...\variable_argument_list_04.py", line 16, in <module>
#    main()
#  File "...\variable_argument_list_04.py", line 5, in main
#    print('2 * 3 * 4 =',my_product2(2,3,4))
#TypeError: my_product2() takes 1 positional argument but 3 were given
