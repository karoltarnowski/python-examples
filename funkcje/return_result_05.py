# -*- coding: utf-8 -*-
"""
Przykładowa funkcja zwracająca trzy wyniki (zapakowane w krotkę).
Dwie pierwsze wartosci są ignorowane przy przypisaniu.
"""

def fun(x,y):
    return x+y, x-y, y-x

a = 4
b = 7

_, _, rb = fun(a,b)
#print("a + b = ", s)
#print("a - b = ", ra)
print("b - a = ", rb)

