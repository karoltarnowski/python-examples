# Program pokazuje przekazywanie argumentów pozycyjnych.
# Liczba argumentów w wywołaniu musi wynosić 3.
def fun(a,b,c):
    print(a)
    print(b)
    print(c)

# fun('a', 'b')
# fun('a', 'b', 'c')
# fun('a', 'b', 'c', 'd')

