# Program pokazuje przekazywanie argumentów nazwanych.
# Liczba argumentów w wywołaniu musi wynosić 3.
def fun(a,b,c):
    print(a)
    print(b)
    print(c)

fun(a = 'a', b = 'b', c = 'c')
fun(b = 'b', c = 'c', a = 'a')






