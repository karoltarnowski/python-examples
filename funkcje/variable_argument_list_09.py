# Program pokazuje funkcję przyjmującą wiele argumentów pozycyjnych.
def fun(*args):
    for item in args:
        print(item)

fun('a', 'b', 'c')
fun('a', 'b', 'c', 'd')
fun('a')
fun()





