#Ten program wyświetla instrukcję krok po kroku
#jak schować słonia do lodówki.

#Funkcja main() zawiera logikę główną programu
def main():
    #Wyświetlenie komunikatu początkowego
    startup_message()
    input('Naciśnij Enter, aby przejść do kroku 1.')
    #Wyświetlenie instrukcji dla kroku 1.
    step1()
    input('Naciśnij Enter, aby przejść do kroku 2.')
    #Wyświetlenie instrukcji dla kroku 2.
    step2()
    input('Naciśnij Enter, aby przejść do kroku 3.')
    #Wyświetlenie instrukcji dla kroku 3.
    step3()
    input('Naciśnij Enter, aby zakończyć.')
    final_message()


#Funkcja startup_message() wyświetla na ekranie
#komunikat początkowy.      
def startup_message():
    print('Ten program przeprowadzi Cię,')
    print('przez proces chowania słonia do lodówki.')
    print('Proces składa się z trzech prostych kroków.')
    print()

#Funkcja step1() wyświetla instrukcję dla kroku 1.
def step1():
    print('Krok 1.')
    print('Otwórz lodówkę.')
    print()

#Funkcja step2() wyświetla instrukcję dla kroku 2.
def step2():
    print('Krok 2.')
    print('Włóż słonia do lodówki.')
    print()

#Funkcja step3() wyświetla instrukcję dla kroku 3.
def step3():
    print('Krok 3.')
    print('Zamknij lodówkę.')
    print()

#Funkcja final_message() wyświetla na ekranie
#komunikat końcowy.
def final_message():
    print('\nJeśli postępowałeś zgodnie z instrukcjami,')
    print('to słoń jest w lodówce.\n')

#Wywołanie funkcji main() i rozpoczęcie działania programu
main()      






