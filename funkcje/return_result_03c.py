#Program oblicza promocyjną cenę produktu. Na podstawie ceny podstawowej
#podanej przez użytkownika. Program wykorzystuje dwie funkcje:
# discount(price)
# get_regular_price()

#Stała DISCOUNT zawiera zniżkę.
DISCOUNT = 0.20

#Funkcja main() zawierająca logikę główną programu
def main():
    #Pobranie podstawowej ceny produktu
    reg_price = get_regular_price()
    #Obliczenie ceny promocyjnej
    sale_price = reg_price - discount(reg_price)
    #Wyświetlenie ceny promocyjnej
    print('Cena promocyjna wynosi ',
          format(sale_price,'.2f'), ' zł.',sep = '')

#Funkcja discount() przyjmuje jako argument podstawową
#cenę produktu i zwraca wysokość rabatu obliczoną
#na podstawie zniżki (stała DISCOUNT)
def discount(price):
    return price*DISCOUNT

#Funkcja get_regular_price() prosi użytkownika o podanie
#ceny podstawowej produktu i zwraca wartość
def get_regular_price():
    price = float(input('Podaj cenę detaliczną produktu: '))
    return price

    
main()













