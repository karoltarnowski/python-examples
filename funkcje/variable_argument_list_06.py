def main():
    print('2 * 3 =',my_product1(2,3))
    print('2 * 3 =',my_product2([2,3]))
    print('2 * 3 * 4 =',my_product2([2,3,4]))
    print('2 * 3 * 4 =',my_product3(2,3,4))
    print('2 * 3 * 4 =',my_product3([2,3,4]))
    print('2 =',my_product3([2]))
    print('2 =',my_product3(2))
    #print('2 =',my_product3())
    #print('2 =',my_product3([]))
   
def my_product1(a,b):
    return a*b

def my_product2(l):
    result = 1
    for x in l:
        result *= x
    return result

# funkcja przyjmuje wiele argumentów 
# i pakuje je w krotkę
def my_product3(*args):
    result = 1
    if len(args) == 0:
        return result
    elif len(args) == 1:
        args = tuple(args[0])
    for x in args:
        result *= x
    return result
        
main()

