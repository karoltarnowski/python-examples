# -*- coding: utf-8 -*-
"""
Przykładowa funkcja zwracająca trzy wyniki (zapakowane w krotkę).
"""

def fun(x,y):
    return x+y, x-y, y-x

a = 4
b = 7

s, ra, rb = fun(a,b) #rozpakowanie krotki do trzech zmiennych
print("a + b = ", s)
print("a - b = ", ra)
print("b - a = ", rb)


