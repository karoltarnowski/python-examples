# -*- coding: utf-8 -*-
"""
Definicja funkcji z argumentem domyślnym
"""

def f(a,b = 0):
    print(a, b)
    
# wartość a dopasowane pozycyjnie
# wartość b nadana domyślna
f(1)

# oba argumenty nadane pozycyjnie
# (zastąpiona wartość domyślna)
f(2,3)

# wartość a dopasowane pozycyjnie
# wartość b nadana jako argument nazwany
f(4,b=5)

# oba argumenty jako argumenty nazwane
f(b=6,a=7)



