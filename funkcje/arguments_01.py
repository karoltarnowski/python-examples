# -*- coding: utf-8 -*-
"""
Przykład ilustruje przekazanie argumentu przez wartość
"""

def f(a):
    a = 99
    
b = 88
f(b)
print("b =", b)
# zmienna b ma wartość 88 mimo wywołania funkcji f(b)


