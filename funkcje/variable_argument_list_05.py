def main():
    print('2 * 3 =',my_product1(2,3))
    print('2 * 3 =',my_product2([2,3]))
    print('2 * 3 * 4 =',my_product2([2,3,4]))
    print('2 * 3 * 4 =',my_product3(2,3,4))
   
def my_product1(a,b):
    return a*b

def my_product2(l):
    result = 1
    for x in l:
        result *= x
    return result

# funkcja przyjmuje wiele argumentów 
# i pakuje je w krotkę
def my_product3(*l):
    #print(type(l))
    result = 1
    for x in l:
        result *= x
    return result
        
main()

