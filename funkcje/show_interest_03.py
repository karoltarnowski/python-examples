#Ten program pokazuje przykład wywołania funkcji
#z użyciem argumentów domyślnych.

#Funkcja główna wywołuje funkcję wyświetlającą obliczony wynik.
def main():
    #W tym wywołaniu argumenty przyjmują wartości domyślnych.
    show_interest()

#Funkcja wyświetlająca wysokość odsetek
#należnych od zdeponowanej kwoty
#w zależności od czasu trwania depozytu
#oraz wysokości oprocentowania.
def show_interest(deposit = 1000, rate = 0.03, periods = 3):
    interest = deposit * rate * periods
    print('Wysokość odsetek wynosi',
          format(interest, '.2f')
          )

#Wywołanie funkcji głównej
main()



