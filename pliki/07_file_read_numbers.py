# Przykładowy program pokazuje
# odczyt danych liczbowych z pliku
# zapisanych w postaci tekstu.

def main():
    print('Program wczytuje trzy liczby całkowite')
    print('z pliku "liczby.txt" i oblicza ich sumę.')

    #otwarcie pliku "liczby.txt" do odczytu
    infile = open('liczby.txt','r')
    #odczytanie 3 linii tekstu
    #zamiana liczb na int
    num1 = int(infile.readline())
    num2 = int(infile.readline())
    num3 = int(infile.readline())
    #zamknięcie pliku
    infile.close()

    #obliczenie sumy liczb
    total = num1 + num2 + num3

    #wyświetlenie liczb i stosowanego komunikatu
    print('Odczytane liczby to:',num1, num2, num3)
    print('Suma liczb wynosi:', total)

main()




