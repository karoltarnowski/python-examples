# Przykładowy program wykonujący
# odczyt z pliku.

def main():
    #otwarcie pliku "nobliści.txt" do odczytu
    infile = open('nobliści.txt','r')

    #wczytanie zawartości pliku metodą readline()
    line1 = infile.readline()
    line2 = infile.readline()
    line3 = infile.readline()
    line4 = infile.readline()
    line5 = infile.readline()

    #zamknięcie pliku
    infile.close()

    #wyświetlenie danych wczytanych do pamięci
    print(line1)
    print(line2)
    print(line3)
    print(line4)
    print(line5)
    

main()



