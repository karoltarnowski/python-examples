# Przykładowy program wykonuje
# zapis danych tekstowych w pliku
# dodając znak nowego wiersza.

def main():
    print('Podaj imiona trójki przyjaciół.')
    name1 = input('Przyjaciel #1: ')
    name2 = input('Przyjaciel #2: ')
    name3 = input('Przyjaciel #3: ')

    #otwarcie pliku "imiona.txt" do zapisu
    outfile = open('imiona.txt','w')
    #zapisanie 3 linii tekstu
    #(dołączony znak nowej linii)
    outfile.write(name1 + '\n')
    outfile.write(name2 + '\n')
    outfile.write(name3 + '\n')
    #zamknięcie pliku
    outfile.close()

main()




