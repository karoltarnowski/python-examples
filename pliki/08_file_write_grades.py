# Przykładowy program pokazuje
# zapis danych liczbowych do pliku
# z wykorzystaniem pętli.

def main():
    print('Program zapisuje do pliku "oceny.txt"')
    print('oceny podane przez użytkownika.')

    number = int(input('Ile ocen chcesz podać? '))

    #otwarcie pliku "oceny.txt" do zapisu
    outfile = open('oceny.txt','w')

    #pobranie od użytkownika ocen
    #i zapisanie ich do pliku.
    for count in range(1,number+1):
        grade = float(input(
            'Podaj ' + str(count) + ' ocenę: '))
        outfile.write(str(grade) + '\n')

    #zamknięcie pliku
    outfile.close()
    
main()




