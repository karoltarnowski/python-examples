# Przykładowy program wykonujący
# odczyt z pliku.

def main():
    #otwarcie pliku "nobliści.txt" do odczytu
    infile = open('nobliści.txt','r')

    #wczytanie zawartości pliku metodą read()
    file_contents = infile.read()

    #zamknięcie pliku
    infile.close()

    #wyświetlenie danych wczytanych do pamięci
    print(file_contents)

main()


