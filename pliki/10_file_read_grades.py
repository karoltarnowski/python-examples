# Przykładowy program pokazuje odczyt
# danych liczbowych do pliku
# z wykorzystaniem pętli for.
# Pętla for "przechodzi" przez plik
# dzięki temu, że obiekt reprezentujący
# plik udostępnia iterator.

def main():
    print('Program odczytuje oceny')
    print('z pliku "oceny.txt".')
    
    #otwarcie pliku "oceny.txt" do zapisu
    infile = open('oceny.txt','r')

    #dla kolejnych linii pliku 
    for line in infile:
        #skonwertuj tekst na liczbę
        amount = float(line)
        #wyświetl ocenę
        print(format(amount,'.1f'))
        
    #zamknięcie pliku
    infile.close()
    
main()





