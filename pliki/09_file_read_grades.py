# Przykładowy program pokazuje odczyt
# danych liczbowych do pliku
# z wykorzystaniem pętli.
# Funkcja readline() zwraca pusty łańcuch
# jeśli osięgnięty zostanie koniec pliku.

def main():
    print('Program odczytuje oceny')
    print('z pliku "oceny.txt".')
    
    #otwarcie pliku "oceny.txt" do zapisu
    infile = open('oceny.txt','r')

    #odczytanie z pliku linii tekstu
    line = infile.readline()

    #dopóki linia nie jest pusta
    while line != '':
        #skonwertuj tekst na liczbę
        amount = float(line)
        #wyświetl ocenę
        print(format(amount,'.1f'))
        #wczytaj kolejną linię
        line = infile.readline()

    #zamknięcie pliku
    infile.close()
    
main()




