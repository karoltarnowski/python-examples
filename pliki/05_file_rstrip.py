# Przykładowy program wykonuje
# odczyt danych tekstowych z pliku (readline),
# i usuwa znak nowej linii (rstrip).

def main():
    print('Program wczytuje imiona trójki przyjaciół.')

    #otwarcie pliku "imiona.txt" do odczytu
    infile = open('imiona.txt','r')
    #odczytanie 3 linii tekstu
    line1 = infile.readline()
    line2 = infile.readline()
    line3 = infile.readline()
    #usunięcie znaków nowej linii
    line1 = line1.rstrip('\n')
    line2 = line2.rstrip('\n')
    line3 = line3.rstrip('\n')
    #zamknięcie pliku
    infile.close()

    print('Przyjaciel #1:',line1)
    print('Przyjaciel #2:',line2)
    print('Przyjaciel #3:',line3)

main()


