# Przykładowy program pokazuje
# zamianę danych liczbowych na tekst
# przed zapisaniem ich do pliku.

def main():
    print('Podaj trzy liczby całkowite')
    num1 = int(input('Pierwsza liczba: '))
    num2 = int(input('Druga liczba: '))
    num3 = int(input('Trzecia liczba: '))
    
    #otwarcie pliku "liczby.txt" do zapisu
    outfile = open('liczby.txt','w')
    #zapisanie 3 liczb po skonwertowaniu
    #na łańcuch tekstowy
    outfile.write(str(num1) + '\n')
    outfile.write(str(num2) + '\n')
    outfile.write(str(num3) + '\n')
    #zamknięcie pliku
    outfile.close()

main()




