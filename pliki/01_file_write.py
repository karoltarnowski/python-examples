# Przykładowy program wykonujący
# zapis do pliku.

def main():
    #otwarcie pliku "nobliści.txt" do zapisu
    outfile = open('nobliści.txt','w')

    #zapisanie w pliku imion i nazwisk
    #polskich laureatów Nagrody Nobla
    #w dziedzinie literatury
    outfile.write('Henryk Sienkiewicz\n')
    outfile.write('Władysław Reymont\n')
    outfile.write('Czesław Miłosz\n')
    outfile.write('Wisława Szymborska\n')
    outfile.write('Olga Tokarczuk\n')

    outfile.close()

main()


