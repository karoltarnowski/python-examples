# -*- coding: utf-8 -*-
"""
Metoda print_num_instances() jest wywoływana
na rzecz konkretnej instancji klasy.
"""

class Spam:
    
    num_instances = 0
    
    def __init__(self):
        Spam.num_instances += 1
    
    def print_num_instances(self):
        print("Liczba utworzonych instancji:",
          Spam.num_instances)

