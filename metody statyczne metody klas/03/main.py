# -*- coding: utf-8 -*-
"""
Przykład pokazuje definicję metody "statycznej" wywoływanej
na rzecz obiektu.
"""

from spam import Spam

a, b, c = Spam(), Spam(), Spam()
a.print_num_instances()
Spam.print_num_instances(a)

# wywołanie konstruktura zmienia liczbę instancji
Spam().print_num_instances()


