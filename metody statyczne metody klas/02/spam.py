# -*- coding: utf-8 -*-
"""
Przykład pokazuje wykorzystanie metody globalnej w module.
"""

def print_num_instances():
    print("Liczba utworzonych instancji:",
          Spam.num_instances)

class Spam:
    
    num_instances = 0
    
    def __init__(self):
        Spam.num_instances += 1
    
