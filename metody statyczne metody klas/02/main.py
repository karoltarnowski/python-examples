# -*- coding: utf-8 -*-
"""
Wywołanie metody zdefiniowane w module "obok" klasy.
Taka metoda nie byłaby dziedziczona przez klasę pochodną.
"""

import spam

a = spam.Spam()
b = spam.Spam()
c = spam.Spam()
spam.print_num_instances()
print(spam.Spam.num_instances)

