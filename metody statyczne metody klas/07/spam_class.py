# -*- coding: utf-8 -*-
"""
"""

class Spam:
    
    num_instances = 0
    
    def __init__(self):
        Spam.num_instances += 1
    
    def print_num_instances(cls):
        print("Liczba utworzonych instancji:",
              cls.num_instances)
        
    print_num_instances = classmethod(print_num_instances)
    
class Sub(Spam):
    def print_num_instances(cls):
        print("dodatek")
        Spam.print_num_instances()
        
    print_num_instances = classmethod(print_num_instances)
        
class Other(Spam):
    pass

def main():
    x = Sub()
    y = Spam()
    
    x.print_num_instances()
    Sub.print_num_instances()
    y.print_num_instances()
    
    z = Other()
    z.print_num_instances()
    
if __name__ == "__main__":
    main()
