# -*- coding: utf-8 -*-
"""
"""

class Spam:
    
    num_instances = 0
    
    def __init__(self):
        Spam.num_instances += 1
    
    def print_num_instances(cls):
        print("Liczba utworzonych instancji:",
              cls.num_instances)
        
    print_num_instances = classmethod(print_num_instances)
    
def main():
    a, b = Spam(), Spam()
    
    a.print_num_instances()
    Spam.print_num_instances()    
    
if __name__ == "__main__":
    main()
