# -*- coding: utf-8 -*-
"""
"""

class Spam:
    
    num_instances = 0
    
    def  __init__(self):
        Spam.num_instances += 1
        
    @staticmethod
    def print_num_instances():
        print("Liczba utworzonych instancji:",Spam.num_instances)

def main():
    a = Spam()
    b = Spam()
    c = Spam()
    Spam.print_num_instances()
    a.print_num_instances()
    
    
if __name__ == "__main__":
    main()
        