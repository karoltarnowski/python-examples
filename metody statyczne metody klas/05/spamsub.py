# -*- coding: utf-8 -*-
"""
"""

class Spam:
    
    num_instances = 0
    
    def __init__(self):
        Spam.num_instances += 1
    
    def print_num_instances():
        print("Liczba utworzonych instancji:",
              Spam.num_instances)
        
    print_num_instances = staticmethod(print_num_instances)
    
class Sub(Spam):
    def print_num_instances():
        print("dodatek")
        Spam.print_num_instances()

    print_num_instances = staticmethod(print_num_instances)
    
class Other(Spam):
    pass
        
def main():
    a = Sub()
    b = Sub()
    
    a.print_num_instances()
    Sub.print_num_instances()
    Spam.print_num_instances()    
    
    c = Other()
    c.print_num_instances()
    
if __name__ == "__main__":
    main()
