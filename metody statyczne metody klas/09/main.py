# -*- coding: utf-8 -*-
"""
"""

from methods import Methods

obj = Methods()

obj.imeth(1)
Methods.imeth(obj, 2)

Methods.smeth(3)
obj.smeth(4)

Methods.cmeth(5)
obj.cmeth(6)