# -*- coding: utf-8 -*-
"""
"""

class Methods:
    
    def imeth(self, x):
        print([self, x])
    
    @staticmethod
    def smeth(x):
        print([x])
    
    @classmethod
    def cmeth(cls, x):
        print([cls, x])