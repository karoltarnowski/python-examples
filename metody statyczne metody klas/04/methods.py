# -*- coding: utf-8 -*-
"""
Klasa impementuje metody:
    - instancji,
    - statyczną,
    - klasową.
"""

class Methods:
    
    # definicja metody instancji
    def imeth(self, x):
        print([self, x])
    
    # definicja metody statycznej
    def smeth(x):
        print([x])
    
    # definicja metody klasowej
    def cmeth(cls, x):
        print([cls, x])
        
    # oznaczenie metody jako statycznej
    smeth = staticmethod(smeth)
    
    # oznaczenie meotdy jako klasowej
    cmeth = classmethod(cmeth)
    
    