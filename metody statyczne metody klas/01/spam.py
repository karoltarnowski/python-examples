# -*- coding: utf-8 -*-
"""
Przykład pokazuje wykorzystanie atrybutu i metody,
które są wspólne dla całej klasy.
"""

class Spam:
    
    # num_instances jest utworzone w klasie
    # a nie w instancji klasy
    num_instances = 0
    
    def __init__(self):
        # powiększenie licznika instancji
        # przez konstruktor
        Spam.num_instances += 1
    
    # metoda print_num_instances() nie przyjmuje
    # argumentu self
    def print_num_instances():
        print("Liczba utworzonych instancji:",
              Spam.num_instances)
        
def main():
    a = Spam()
    b = Spam()
    c = Spam()
    Spam.print_num_instances()
    # metody zdefiniowanej w ten sposób nie
    # można wywołać, na rzecz obiektu klasy.
    # a.print_num_instances()
    # Spam.print_num_instances(a)
    
    
if __name__ == "__main__":
    main()


