# -*- coding: utf-8 -*-
"""
Przykład prostego generatora
utworzonego jako funkcja generująca.
"""

#funkcja generująca
def g(n):
    for x in range(n):
        yield x*x #słowo kluczowe yield 
        
#for i in g(10):
#    print(i)


