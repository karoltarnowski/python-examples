# -*- coding: utf-8 -*-
"""
Przykład pokazujący działanie generatora
utworzonego przez wyrażenie generujące.
"""

#utworzenie generatora z wykorzystaniem wyrażenia generującego
g = (x*x for x in range(3))

#odczytanie wartości generatora
print(next(g))
input()

#kolejne odczytanie wartości generatora
print(next(g))
input()

#kolejne odczytanie wartości generatora
print(next(g))
input()

#to odczytanie wartości generatora skutkuje błędem
print(next(g))


