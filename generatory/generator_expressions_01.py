# -*- coding: utf-8 -*-
"""
Przykładowy skrypt pozwalający porównać
wyrażenia listowe oraz wyrażenia generujące
"""

#lista kwadratów liczb
list1 = [x*x for x in range(10)]
print(list1)
#suma elementów tej listy
print(sum(list1))

#suma elementów listy utworzonej przez wyrażenie listowe
print(sum([x*x for x in range(10)]))

#suma obliczona z wykorzystaniem wyrażenia generującego
print(sum((x*x for x in range(10))))

#wyrażenie generujące
g = (x*x for x in range(10))



