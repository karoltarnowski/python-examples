#Program demonstruje działanie funkcji factorial
#obliczającej wartość silni rekurencyjnie

def main():
    print('5! =',factorial(5))

def factorial(n, level = 0):
    print(f"level = {level}", level*"   ", f"factorial({n})")
    if n == 0:
        print(f"level = {level}", level*"   ", 1)
        return 1
    else:
        x = n*factorial(n-1, level + 1)
        print(f"level = {level}", level*"   ", x)
        return x

main()




