#Program demonstruje działanie funkcji factorial
#obliczającej wartość silni rekurencyjnie

def main():
    print('5! =',factorial(5))

#I :liczba całkowita n
#P :oblicz silnię liczby n jako iloczyn
#   n * silnia (n-1) (jeśli n jest różne od zera)
#   dla zera - warunek stopu
#   0! = 1
#O :silnia liczby n

def factorial(n):
    if n == 0:
        return 1
    else:
        return n*factorial(n-1)

main()




