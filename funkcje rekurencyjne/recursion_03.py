#Program pokazuje przykładową funkcję rekurencyjną.

def main():
    message(5)

#I :liczba całkowita określająca liczbę wywołań
#   rekurencyjnych
#P :jeśli liczba wywołań jest dodatnia
#       wywołaj rekurencyjnie funkcję message()
#           z liczbę wywołań mniejszą o jeden
#       wyświetl informację
#O :brak
def message(times):
    if times > 0:
        #zmieniona kolejność instrukcji
        message(times - 1)
        print('Wartość parametru times', times)

main()




