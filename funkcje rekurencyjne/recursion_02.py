#Program pokazuje przykładową funkcję rekurencyjną.

def main():
    message(5)

#I :liczba całkowita określająca liczbę
#   wywołań rekurencyjnych
#P :jeśli liczba wywołań jest dodatnia
#       wyświetl informację
#       wywołaj rekurencyjnie funkcję message()
#           z liczbę wywołań mniejszą o jeden
#O :brak
def message(times):
    if times > 0:
        print('Wartość parametru times', times)
        message(times - 1)

main()




