#Program pokazuje przykładową funkcję rekurencyjną.

#Jest to przykład nieskończonej rekurencji.

def main():
    message()

#Funkcja message() wywołuje samą siebie.
def message():
    print('To jest funkcja rekurencyjna')
    message()

main()




