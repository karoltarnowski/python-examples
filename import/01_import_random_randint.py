# Program pokazuje importowanie modułu (random)
# w celu wywołania funkcji zdefiniowanej w nim (randint).
# Program pokazuje wywołanie funkcji randint().
# Funkcja randint(a, b) zwraca losową liczbę całkowitą
# z przedziału od a do b (włącznie).

# Program symuluje pięć rzutów kostką sześcienną.

import random #import modułu

def main():
    #wykonaj pięć razy
    for i in range(5):
        #rzut kostką (losowa liczba od 1 do 6)
        number = random.randint(1,6)
        #wyświetl wynik
        print(number)

main()
