# Program pokazuje działanie funkcji sqrt()
# z modułu math.

#importowanie modułu math
import math

def main():
    #wczytanie liczby rzeczywistej
    number = float(input('Podaj liczbę: '))
    #obliczenie pierwiastka
    #funkcja sqrt() z biblioteki math
    square_root = math.sqrt(number)
    #wyświetlenie wyników
    print('Pierwiastek kwadratowy liczby',
          number,'wynosi',square_root)
    
main()


