# Program symuluje serię rzutów monetą.
# Program wykorzystuje funkcję randint()
# z modułu random.

import random #import modułu

HEAD = 1    #stała oznaczająca awers (orzeł)
TAIL = 2    #stała oznaczająca rewers (reszka)
TOSSES = 10 #stała określająca liczbę rzutów

def main():
    # w każdym rzucie
    for toss in range(TOSSES):
        # wylosuj liczbę (1 lub 2)
        # 1 oznacza awers (orzeł)
        if random.randint(HEAD, TAIL) == HEAD:
            print('orzeł')
        # w przeciwnym wypadku to rewers (reszka)
        else:
            print('reszka')
            
main()
