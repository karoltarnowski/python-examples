# Program pokazuje działanie funkcji seed().
# Program symuluje pięć rzutów kostką sześcienną.
# Wartości uzyskiwane z każdego uruchomienia
# są takie same - dzięki ustawieniu ziarna generatora.

import random

def main():
    # ustawienie ziarna generatora
    random.seed(10)
    for i in range(5):
        print(random.randint(1,6))
    
main()
