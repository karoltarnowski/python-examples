# Program oblicza pole koła o podanym promieniu.
# Program wykorzystuje wielkość pi z modułu math.

import math

def main():
    #wczytanie liczby rzeczywistej
    radius = float(input('Podaj promień koła: '))
    area   = math.pi * radius ** 2
    #wyświetlenie wyników
    print('Pole koła o promieniu',
          radius,'wynosi',area)
    
main()


