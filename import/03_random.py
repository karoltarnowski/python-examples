# Program pokazuje różne funkcje z modułu random.

import random

def main():
    #randrange - zwraca losową liczbę z listy 
    print(random.randrange(6))        #losowa wartość od 0 do 5
    print(random.randrange(7,10))     #losowa wartość od 7 do 9
    print(random.randrange(0,101,10)) #losowa wartość z listy od 0 co 10 do 100
    #random - zwraca losową liczbę z przedziału [0.0, 1.0)
    print(random.random())            
    #uniform - zwraca losową liczbę z przedziału [a, b]
    print(random.uniform(3.0,5.0))    #losowa wartość z przedziału [3, 5]
    
main()
