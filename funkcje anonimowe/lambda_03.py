# -*- coding: utf-8 -*-
"""
Przykład pokazujący funkcję anonimową
wykorzystujący instrukcję warunkową.
"""

# definicja funkcji anonimowej
# (funkcja przypisana do zmiennej smaller)
smaller = lambda x,y : x if x < y else y

# wywołanie funkcji anonimowej
for i in [1, 3, 5, 7, 4, 8, 2, 6]:
    print(smaller(i,5))

    
    
    
    