# -*- coding: utf-8 -*-
"""
Przykład pokazujący użycie funkcji anonimowej.
"""

# definicja funkcji anonimowej (funkcja przypisana do zmiennej f)
f = lambda x : x*x

# wywołanie funkcji anonimowej (przechowywanej w zmiennej f)
for i in range(10):
    print(f(i))

# funkcja anonimowa zdefiniowana w miejscu wywołania
for i in range(10):
    print((lambda x : x*x)(i))


