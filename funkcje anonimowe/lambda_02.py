# -*- coding: utf-8 -*-
"""
Przykład pokazujący wykorzystanie funkcji anonimowej
przyjmującej dwa argumenty.
"""

b = 2
# definicja funkcji anonimowej
#multipler = lambda x,y : x*y

doubler = lambda x : x*b

# wywołanie funkcji anonimowej (przechowywanej w zmiennej f)
for i in range(10):
    print(doubler(i))
    
    
    
    