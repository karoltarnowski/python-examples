#Program pokazuje obsługę dwóch typów wyjątków
#na przykładzie dzielenia dwóch liczb.

def main():
    try:
        num1 = int(input('Podaj liczbę: '))
        num2 = int(input('Podaj następną liczbę: '))

        result = num1 / num2
        print(num1, 'dzielone przez', num2, 'daje', result)
        
    except ValueError:
        print('Niepoprawne dane wejściowe.')

    except ZeroDivisionError:
        print('Błąd dzielenia przez 0.')

main()
    
