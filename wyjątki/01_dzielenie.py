#Program pokazuje sytuację, w której możliwe jest
#zgłoszenie wyjątku związanego z dzieleniem przez 0.

def main():
    num1 = int(input('Podaj liczbę: '))
    num2 = int(input('Podaj następną liczbę: '))

    #dzielenie nie będzie wykonane jeśli num2 == 0
    result = num1 / num2
    print(num1, 'dzielone przez', num2, 'daje', result)

main()
    
