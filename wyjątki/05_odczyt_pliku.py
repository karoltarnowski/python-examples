#Program pokazuje inną sytuację, w której zgłaszany
#jest wyjątek.

def main():
    try:
        filename = input('Podaj nazwę pliku: ')
        #funkcja open() zwraca wyjątek typu IOError
        infile = open(filename,'r')
        contents = infile.read()
        print(contents)
        infile.close()
        
    except IOError:
        #wyjątek jest obsłużony - wyświetlono komunikat
        print('Wystąpił błąd podczas próby odczytania')
        print('pliku o nazwie', filename)

main()
    
