#Program pokazuje inną sytuację, w której zgłaszany
#jest wyjątek.

def main():
    filename = input('Podaj nazwę pliku: ')
    
    #funkcja open() zgłasza wyjątek, jeśli
    #nie istnieje plik o podanej nazwie
    infile = open(filename,'r')

    contents = infile.read()
    print(contents)
    infile.close()

main()
    
