#Program pokazuje obsługę dwóch typów wyjątków
#na przykładzie dzielenia dwóch liczb.
#Program wypisuje domyślny komunikat wyjątku.

def main():
    try:
        num1 = int(input('Podaj liczbę: '))
        num2 = int(input('Podaj następną liczbę: '))

        result = num1 / num2
        print(num1, 'dzielone przez', num2, 'daje', result)
        
    #wszystkie typy wyjątków "pasują" do typu Exception
    except Exception as err:
        print(err)    

main()
    
