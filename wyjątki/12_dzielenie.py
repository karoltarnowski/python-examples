#Program pokazuje działanie konstrukcji:
#try-except-else

def main():
    try:
        num1 = int(input('Podaj liczbę: '))
        num2 = int(input('Podaj następną liczbę: '))
        result = num1 / num2
        
    except Exception as err:
        print(err)

    else:
        print(num1, 'dzielone przez', num2, 'daje', result)


main()
    
