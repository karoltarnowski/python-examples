#Program pokazuje obsługę wyjątków różnych typów

def main():
    total = 0.0
    
    try:
        infile = open('sales1_data.txt','r')
        for line in infile:
            amount = float(line)
            total += amount

    except Exception as err:
        print(err)

    else:
        print(format(total, '.2f'))

    finally:
        print('Zamknięto plik.')
        infile.close()


main()
    
