#Program pokazuje sytuację, w której uniknięto
#zgłoszenia wyjątku ZeroDivisionError.

def main():
    num1 = int(input('Podaj liczbę: '))
    num2 = int(input('Podaj następną liczbę: '))

    if num2 != 0:
        result = num1 / num2
        print(num1, 'dzielone przez', num2, 'daje', result)
    else:
        print('Nie można dzielić przez zero.')

main()
    
