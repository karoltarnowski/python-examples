#Program pokazuje obsługę wyjątków różnych typów.
#Program podsumowuje liczby zestawione w pliku
#sales_data.txt.

def main():
    total = 0.0
    
    try:
        #wyjątek IOError może być zgłoszony
        #przy otwarciu pliku
        infile = open('sales_data.txt','r')
        for line in infile:
            #wyjątek ValueError może być zgłoszony
            #przy konwertowaniu danych tekstowych
            amount = float(line)
            total += amount

        infile.close()
        print(format(total, '.2f'))
        
    except IOError:
        print('Wystąpił błąd podczas odczytu pliku')

    except ValueError:
        print('W pliku znajdują się dane inne niż liczbowe')

    except:
        print('Wystąpił błąd.')

main()












    
