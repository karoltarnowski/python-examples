#Program pokazuje obsługę wyjątku ValueError
#zgłoszonego przez funkcję int().

def main():
    try:
        #jeśli napisu nie można zamienić na liczbę całkowitą
        #to jest zgłaszany wyjątek ValueError
        num1 = int(input('Podaj liczbę: '))
        num2 = int(input('Podaj następną liczbę: '))

        if num2 != 0:
            result = num1 / num2
            print(num1, 'dzielone przez', num2, 'daje', result)
        else:
            print('Nie można dzielić przez zero.')
            
    except ValueError:
        print('Błąd!')

main()
    
