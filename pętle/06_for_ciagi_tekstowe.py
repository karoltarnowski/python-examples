#Program pokazuje prostą pętlę for.

#Program wyświetla ciągi tekstowe z listy.
#Lista może zawierać ciągi tekstowe jako elementy.

def main():
    print('Atomówki to:')
    for name in ['Bajka', 'Bójka', 'Brawurka']:
        print(name)
        
main()



