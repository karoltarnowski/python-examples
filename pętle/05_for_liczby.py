#Program pokazuje prostą pętlę for.

#Program wyświetla liczby z listy: [1, 3, 5, 7, 9]

def main():
    print('Wyświetlam liczby nieparzyste od 1 do 9')
    for num in [1, 3, 5, 7, 9]:
        print(num)
        
main()



