# Program pokazuje porównanie pętli while oraz for
# na przykładzie wypisywania ciagu tekstowego.

# Dodatkowo pokazuje także działanie instrukcji
# break oraz continue w obu pętlach.

def main():
    text = 'napis'

    print('while_example(text)')
    while_example(text)
    input()

    print('for_example_1(text)')
    for_example_1(text)
    input()

    print('for_example_2(text)')
    for_example_2(text)
    input()

    print('while_break_example(text)')
    while_break_example(text)
    input()

    print('for_break_example(text)')
    for_break_example(text)
    input()

    print('while_continue_example(text)')
    while_continue_example(text)
    input()

    print('for_continue_example(text)')
    for_continue_example(text)



def while_example(text):
    i = 0
    while i < len(text):
        print(i, text[i])
        i = i+1
        #i = i+1
   
def for_example_1(text):
    for c in text:
        print(c)

def for_example_2(text):
    for i in range(len(text)):
        print(i, text[i])
        #i = i+1

def while_break_example(text):
    i = 0
    while i < len(text):
        if text[i] == 'p':
            break
        print(text[i])
        i = i+1

def for_break_example(text):
    for c in text:
        if c == 'p':
            break
        print(c)

def while_continue_example(text):
    i = 0
    while i < len(text):
        if text[i] == 'p':
            i = i+1
            continue
        print(text[i])
        i = i+1

def for_continue_example(text):
    for c in text:
        if c == 'p':
            continue
        print(c)


main()
