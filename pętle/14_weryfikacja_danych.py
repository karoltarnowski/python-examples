#Program pokazuje przykład weryfikacji danych wejściowych.
#
#Program oblicza średnią ocen.
#Przerywa działanie gdy użytkownik wprowadzi 0 jako ocenę.
#Program weryfikuje poprawność danych (jako oceny przyjmuje liczby:
# 2, 3, 3.5, 4, 4.5, 5, 5.5).

def main():
    print('Program oblicza średnią ocen.')
    print('Aby zakończyć działanie programu podaj 0 jako ocenę.')
    print('Średnia ocen wynosi',average_grade())

# I: brak
# P: funkcja wczytuje dane od użytkownika obliczając jednocześnie
#    sumę wszystkich ocen oraz licząc ich liczbę;
#    pętla while jest kontynuowana dopóki oceną nie będzie 0;
#    zwracana wartość to iloraz sumy ocen i ich liczby
#    instrukcja if pozwala uniknąć dzielenia przez 0 jeśli
#    nie wczytano żadnych danych
# O: średnia ocen
def average_grade():
    #inicjalizacja zmiennych
    total   = 0.0 #suma wczytanych ocen
    counter = 0   #liczba wczytanych ocen

    #odczytanie pierwszej oceny
    grade   = get_grade()

    #pętla jest kontynowana jeśli ocena jest różna od zera
    while grade != 0.0:
        total   += grade    #wczytana ocena jest dodawana do sumy
        counter += 1        #oraz zwiększany jest licznik ocen
        print(counter,grade,total) 
        grade = get_grade() #wczytywana jest kolejna ocena

    #zakończenie obliczeń
    #średnia obliczana jako suma ocen dzielona przez ich liczbę
    #if zapobiega dzieleniu przez 0
    if counter != 0:
        return total/counter
    else:
        return 

# I: brak
# P: wczytanie oceny (jako float)
#    jako prawidłowe dane przyjmowane są poprawne oceny lub wartość 0
# O: wczytana ocena
def get_grade():
    grade = float(input('Podaj ocenę: '))
    # sprawdzaj, czy wczytana liczba jest poprawną oceną lub zerem
    # jeśli nie poproś o liczbę jeszcze raz
    while not (is_grade(grade) or grade == 0.0):
        print('Podana liczba nie jest oceną!')
        grade = float(input('Podaj ocenę: '))
    return grade

# I: liczba
# P: prawidłowa ocena jest jedną z wartości: 2, 3, 3.5, 4, 4.5, 5, 5.5;
# O: wartość logiczna - czy liczba jest oceną
def is_grade(number):
    return number == 2.0 or \
           number == 3.0 or number == 3.5 or \
           number == 4.0 or number == 4.5 or \
           number == 5.0 or number == 5.5

           
main()































