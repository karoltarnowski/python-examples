#Program pokazuje użycie klasy range() do generowania listy.

#Program wyświetla liczby: [0, 1, 2, 3, 4]
#Wartości są generowane przez obiekt klasy range().

def main():
    print('Wyświetlam liczby od 0 do 4')
    for num in range(5):
        print(num)
        
main()



