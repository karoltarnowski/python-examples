#Program pokazuje pętlę nieskończoną.
#
#Program pobiera od użytkownika przyspieszenie
#w ruchu jednostajnie przyspieszonym i oblicza drogę przebytą przez ciało
#we wskazanym czasie (zakłada się ruch bez prędkości początkowej).
#Program wykorzystuje funkcje get_acceleration(), get_time(),
#calculate_displacement().

def main():
    a = get_acceleration()

    keep_going = 't' #nadano keep_going wartość 't'

    while keep_going == 't': #warunek jest prawdziwy
        t = get_time()
        s = calculate_displacement(a,t)
        print('W czasie',t,'s','ciało przebyło drogę',s,'m.')
        
        #wartość keep_going nie zmienia się nigdzie w pętli
        #czyli warunek keep_going == 't' będzie zawsze spełniony
        #(wykonanie pętli można przerwać skrótem klawiaturowym Ctrl + C)
        
#I: brak
#P: pobranie od użytkownika przyspieszenia i konwersja na float
#O: wartość przyspieszenia
def get_acceleration():
    return float(input('Podaj przyspieszenie [m/s^2]: '))

#I: brak
#P: pobranie od użytkownika czasu i konwersja na float
#O: czas
def get_time():
    return float(input('Podaj czas [s]: '))

#I: przyppieszenie i czas
#P: obliczenie drogi według wzoru 1/2*a*t**2
#O: droga
def calculate_displacement(a, t):
    return .5*a*t**2

main()
