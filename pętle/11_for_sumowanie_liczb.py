#Program pokazuje przykład użycia pętli for.
#
#Program sumuje 5 liczb podanych przez użytkownika.

MAX = 5

def main():
    print('Program sumuje',MAX,'liczb podanych przez użytkownika.')

    total = 0.0
    for counter in range(MAX):
        number = float(input('Podaj liczbę: '))
        total  = total + number
        
    print('Suma wynosi ',total)

main()


