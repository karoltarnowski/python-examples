#Program ilustrujący działanie pętli for.
#Program wypisuje liczby od 0 do 59.
#Imituje odliczanie stopera.

def main():
    for seconds in range(60):
        print(seconds)

main()
