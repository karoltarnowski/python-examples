#Program pokazuje przykład użycia
#złożonego operatora dodawania.
#
#Program sumuje 5 liczb podanych przez użytkownika.

MAX = 5

def main():
    print('Program sumuje',MAX,'liczb podanych przez użytkownika.')

    total = 0.0
    for counter in range(MAX):
        number = float(input('Podaj liczbę: '))
        #złożony operator dodawania polecenie
        #total = total + number
        total += number

    print('Suma wynosi ',total)

main()


