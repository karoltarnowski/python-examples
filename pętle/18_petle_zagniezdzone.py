#Program "rysuje" na ekranie schodki o n stopniach.

def main():
    print('Program wyświetla schodki o n stopniach')
    n = int(input('Podaj n (dodatnią liczbę całkowitą): '))
    stairs(n)

# I: funkcja pobiera liczbę całkowitą
# P: funkcja wypisuje n wierszy
#    w zerowym wierszu wypisuje zero spacji i znak '#'
#    w pierwszym wierszy jedną spację i znak '#'
#    w kolejnych wierszach zawsze o jedną spację więcej
#    w ten sposób powstają schodki
# O: funkcja nie zwraca wartości
def stairs(n):
    #pętla po wierszach
    for row in range(n):
        #wypisanie spacji
        #liczba spacji odpowiada - numerowi wiersza
        for col in range(row):
            print(' ', end='')
        #wypisanie końcowego znaku '#'
        print('#')

main()





