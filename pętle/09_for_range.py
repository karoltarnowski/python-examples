#Program pokazuje użycie pętli for i funkcji range()
#do wygenerowania tabeli danych.

#Program wyświetla tabelę prędkości w metrach na sekundę
#(od 10 do 50 co 10) z przeliczeniem na kilometry na godzinę.

RATIO = 3.6 # 1 m/s = (1/1000 km) / (1/3600 h) = 3.6 km/h

def main():
    print('v[m/s]\tv[km/h]')
    for v in range(10,60,10):
        print(v,mps_to_kmph(v),sep='\t')

# I: prędkość w metrach na sekundę
# P: mnożenie prędkości przez przelicznik (1 m/s = 3.6 km/h)
# O: prędkość w kilometrach na godzinę
def mps_to_kmph(v): #meter per second to kilometers per hour
    return v*RATIO

main()

