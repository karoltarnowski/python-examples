#Program "rysuje" na ekranie schodki o n stopniach.

def main():
    print('Program wyświetla schodki o n stopniach')
    n = int(input('Podaj n (dodatnią liczbę całkowitą): '))
    stairs(n)

# I: funkcja pobiera liczbę całkowitą
# P: funkcja wypisuje n wierszy (schodków)
#    schodki wypisywane są w funkcji single_stair()
# O: funkcja nie zwraca wartości
def stairs(n):
    for row in range(n):
        single_stair(row)

# I: funkcja pobiera liczbę całkowitą (numer schodka)
# P: funkcja wypisuje n spacji oraz znak '#'
# O: funkcja nie zwraca wartości
def single_stair(n):
    for x in range(n):
        print(' ', end='')
    print('#')
        

main()
