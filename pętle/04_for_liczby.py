#Program pokazuje prostą pętlę for.

#Program wyświetla liczby z listy: [1, 2, 3, 4, 5]
#Elementy listy umieszczone są w nawiasach kwadratowych.

def main():
    print('Wyświetlam liczby od 1 do 5')
    for num in [1, 2, 3, 4, 5]:
        print(num)
        input()
        
main()
