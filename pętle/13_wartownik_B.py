#Program pokazuje przykład użycia wartownika.
#
#Program oblicza średnią ocen.
#Przerywa działanie gdy użytkownik wprowadzi 0 jako ocenę.

def main():
    print('Program oblicza średnią ocen.')
    print('Aby zakończyć działanie programu podaj 0 jako ocenę.')
    print('Średnia ocen wynosi',average_grade())

# I: brak
# P: funkcja wczytuje dane od użytkownika obliczając jednocześnie
#    sumę wszystkich ocen oraz licząc ich liczbę;
#    pętla while jest kontynuowana dopóki oceną nie będzie 0;
#    zwracana wartość to iloraz sumy ocen i ich liczby
#    instrukcja if pozwala uniknąć dzielenia przez 0 jeśli
#    nie wczytano żadnych danych
# O: średnia ocen
def average_grade():
    #inicjalizacja zmiennych
    total   = 0.0 #suma wczytanych ocen
    counter = 0   #liczba wczytanych ocen

    grade   = True #zmienna grad steruje przebiegiem pętli

    #pętla jest kontynuowana, jeśli ocena jest różna od zera
    while grade:
        grade = float(input('Podaj ocenę: '))
        if grade:
            total   += grade    #wczytana ocena jest dodawana do sumy
            counter += 1        #oraz zwiększany jest licznik ocen
        else:
            print("OK. Kończę liczenie średniej")
        print(counter,grade,total) 

    #zakończenie obliczeń
    #średnia obliczana jako suma ocen dzielona przez ich liczbę
    #if zapobiega dzieleniu przez 0
    if counter:
        # w tym przypadku obliczamy średnią
        total /= counter 
    else:
        # w tym przypadku wynikiem jest NaN
        total = float('NaN')

    #zwracamy obliczoną średnią
    return total

main()































