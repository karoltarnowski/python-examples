#Program pokazuje użycie pętli for i klasy range()
#z danymi pochodzącymi od użytkownika.

#Program wyświetla liczby całkowite od podanej liczby początkowej
#mniejsze od zadanego ograniczenia z podanym krokiem.

def main():
    print('Program wyświetla liczby całkowite od podanej liczby początkowej')
    print('mniejsze od zadanego ograniczenia z podanym krokiem.')
    init = int(input('Podaj liczbę początkową: '))
    end  = int(input('Podaj ograniczenie: '))
    step = int(input('Podaj krok: '))
    for i in range(init, end, step):
        print(i)

main()



