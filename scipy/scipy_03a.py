# -*- coding: utf-8 -*-
"""
Przykład pokazujący interpolację danych.
Znane wartości funkcji sinus w węzłach
są interpolowane funkcją interp z biblioteki scipy
oraz klasą CubicSpline z modułu scipy.interpolate
https://docs.scipy.org/doc/scipy/tutorial/interpolate/1D.html
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import CubicSpline

# dane węzłów interpolacji
x = np.array([0, 1, 3, 5, 6, 7, 9, 11, 12])/6*np.pi
y = np.sin(x)

# siatka punktów, dla których będą obliczane
# wartości funkcji interpolujących
x_int = np.linspace(0, 2, 100)*np.pi

# wyznaczenie funkcji interpolujących
# domyślnie liniowej funkcji sklejanej
y_int = np.interp(x_int, x, y)
# funkcji sklejanej sześciennej
spl = CubicSpline(x, y)


# utworzenie wykresów
fig, ax = plt.subplots()
ax.plot(x, y, 'o', label="węzły interpolacji")
ax.plot(x_int, y_int, label="interpolacja liniowa")
ax.plot(x_int, spl(x_int), label="interpolacja sześcienna")
ax.legend()



