# -*- coding: utf-8 -*-
"""
Przykład pokazujący rozwiązywanie zagadnienia
początkowego z użyciem funkcji solve_bvp
z modułu integrate biblioteki scipy.
https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.solve_bvp.html
"""

import numpy as np
from scipy.integrate import solve_bvp
import matplotlib.pyplot as plt

# Definicja równania różniczkowego
def ode(x, y, p):
    """
    Układ równań różniczkowych przekształcony
    na postać równań pierwszego rzędu.
    y[0] = u, y[1] = du/dx
    """
    k = p[0]  # Parametr (k)
    return np.vstack((y[1], -k**2 * y[0]))

# Definicja warunków brzegowych
def bc(ya, yb, p):
    """
    Warunki brzegowe:
    u(0) = 0
    u(1) = 0
    """
    return np.array([ya[0], yb[0], ya[1] - p[0]])

# Siatka początkowa (przedział x od 0 do 1)
x = np.linspace(0, 1, 100)

# Zgadywanie początkowe dla funkcji u(x) i jej pochodnej du/dx
y_guess = np.vstack((x, np.ones_like(x)))  # y[0] = x, y[1] = du/dx

# Początkowe zgadywanie dla wartości k
k_guess = 9.0

# Rozwiązanie za pomocą solve_bvp
sol = solve_bvp(ode, bc, x, y_guess, p=[k_guess])

# Sprawdzenie sukcesu
if sol.success:
    print(f"Rozwiązanie zakończone sukcesem. Znalezione k: {sol.p[0]}")
else:
    print("Rozwiązanie nie powiodło się.")

# Wizualizacja rozwiązania
plt.plot(sol.x, sol.y[0], label=f"u(x), k = {sol.p[0]:.4f}")
plt.title("Rozwiązanie równania różniczkowego")
plt.xlabel("x")
plt.ylabel("u(x)")
plt.axhline(0, color="black", linewidth=0.8, linestyle="--")
plt.legend()
plt.grid(True)
plt.show()



