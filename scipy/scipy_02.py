# -*- coding: utf-8 -*-
"""
Przykład pokazuje użycie funkcji j0 oraz jn_zeros
z modułu special z biblioteki scipy
do obliczania wartości funkcji Bessela.
https://docs.scipy.org/doc/scipy/tutorial/special.html
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy import special

# skalowanie funkcji Bessela
def scaled_bessel(x, k):
    a = special.jn_zeros(0, k)[-1]
    return special.j0(a*x)

# utworzenie siatki punktów 
# na odcinku jednostkowym
x = np.linspace(0,1)

# utworzenie wykresów skalowanych funkcji Bessela
for k in range(1, 6):
    plt.plot(x, scaled_bessel(x, k), 
             label="k = " + format(k,"d"))

plt.legend()














