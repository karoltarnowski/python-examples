# -*- coding: utf-8 -*-
"""
Przykład pokazujący aproksymację danych
funkcją liniową.
Dopasowanie wyznaczane z użyciem
funkcji curve_fit z modułu optimize
biblioteki scipy.
https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.curve_fit.html
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

# definicja funkcji liniowej
# x - argument
# a - współczynnik kierunkowy
# b - wyraz wolny
#def fun(x, a, b):
#    return a*x + b
# równoważna definicja z wykorzystaniem
# funkcji anonimowej
fun = lambda x, a, b : a*x + b

# utworzenie danych do aproksymacji
# dane obliczone z wykorzystaniem
# funkcji liniowej z dodanym szumem
x = np.linspace(0, 10, 100)
y = fun(x, 1, 2) # y = 1*x + 2
yn = y + np.random.normal(size=len(x))

# dopasowanie do danych (x, yn) 
# funkcja curve_fit znajduje parametry
# funkcji fun, które najlepiej dopasowywują ją
# do danych wejściowych
p_opt, _ = curve_fit(fun, x, yn)
print(p_opt)

# utworzenie wykresów
fig, ax = plt.subplots()
# danych wejściowych
ax.plot(x, yn, 'o', label="dane")
# danych dopasowanych
ax.plot(x, fun(x, *p_opt), label="dopasowanie")
ax.legend()


