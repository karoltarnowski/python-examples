# -*- coding: utf-8 -*-
"""
Przykład pokazujący interpolację danych.
Znane wartości funkcji sinus w węzłach
są interpolowane z funkcji interp1d
z modułu interpolate biblioteki scipy.
Funkcja interp1d pozwala wyznaczyć
funkcję interpolującą.
https://docs.scipy.org/doc/scipy/reference/generated/scipy.interpolate.interp1d.html
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d

# dane węzłów interpolacji
x = np.array([0, 1, 3, 5, 6, 7, 9, 11, 12])/6*np.pi
y = np.sin(x)

# wyznaczenie funkcji interpolujących
# domyślnie liniowej funkcji sklejanej
f_lin = interp1d(x, y)
# funkcji sklejanej kwadratowej
f_qua = interp1d(x, y, kind="quadratic")
# funkcji sklejanej sześciennej
f_cub = interp1d(x, y, kind="cubic")

# siatka punktów, dla których będą obliczane
# wartości funkcji interpolujących
x_int = np.linspace(0, 2, 100)*np.pi

# utworzenie wykresów
fig, ax = plt.subplots()
ax.plot(x, y, 'o', label="węzły interpolacji")
ax.plot(x_int, f_lin(x_int), label="interpolacja liniowa")
ax.plot(x_int, f_qua(x_int), label="interpolacja kwadratowa")
ax.plot(x_int, f_cub(x_int), label="interpolacja sześcienna")
ax.legend()



