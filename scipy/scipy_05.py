# -*- coding: utf-8 -*-
"""
Przykład pokazujący rozwiązywanie zagadnienia
początkowego z użyciem funkcji solve_ivp
z modułu integrate biblioteki scipy.
https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.solve_ivp.html
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp

# prawa strona równania różniczkowego
# dg/dt = f(g, t)
# f(t, g) = -g/tau
tau = 1
f = lambda t, g : -g/tau

# zakres zmiennej niezależnej
t = np.array([0, 5])
# wektor zmiennej niezależnej
t_eval = np.linspace(*t)
# obiekt sol zawieta obliczone rozwiązanie
# dla podanego równania róźniczkowego (f)
# z zadanym warunkiem początkowym (g(0)=100)
# dla podanych wartoci zmiennej niezależnej t_eval
sol = solve_ivp(f, t, [100], t_eval = t_eval)

# wykres obliczonego rozwiązania
plt.plot(sol.t,sol.y[0])

















