# -*- coding: utf-8 -*-
"""
Przykład pokazuje użycie funkcji jv
z modułu special z biblioteki scipy
do obliczania wartości funkcji Bessela.
https://docs.scipy.org/doc/scipy/tutorial/special.html
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy import special

# siatka argumentów
x = np.linspace(0,10)

# tablica rzędów funkcji Bessela
nu = np.array([0, 1, 2, 3])

# wykorzystanie funkcji meshgrid
# do wygenerowania dwuwymiarowych tablic:
# rzędów (nus) oraz argumentów (xs)
nus, xs = np.meshgrid(nu, x)

# obliczenie funkcji Bessela
bessel = special.jv(nus, xs)

# stworzenie wykresów 
lines = plt.plot(xs, bessel)

# utworzenie legendy
plt.legend(lines,("nu = " + format(n) for n in nu))
#plt.legend(lines,(r"$\nu$ = " + f"{n}" for n in nu))
#plt.legend(lines,(f"$J_{n}$" for n in nu))









