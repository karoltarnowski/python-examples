# Program pokazuje przetwarzanie listy.
# Program oblicza średnią liczb z listy.

def main():
    data = get_data(5)    
    print('Średnia danych z listy to',average(data))

#funkcja zwraca średnią z elementów listy data
def average(data):
    total = 0
    for value in data:
        total += value
    avg = total / len(data)
    return avg

#I: liczba n reprezentująca długość listy
#P: wczytanie n liczb rzeczywistych i dodanie ich
    #do listy
#O: wczytana lista danych
def get_data(n):
    data = []
    for i in range(n):
        data.append(float(input('Podaj liczbę: ')))
    return data


main()












