# -*- coding: utf-8 -*-
"""
Przykłady użycia wyrażenia listowego
do utworzenia listy list.
"""

matrix = [[i + j for j in range(5) ] for i in range(7)]
print(matrix)

matrix2 = [["*" for j in range(i)] for i in range(5)]
print(matrix2)

