# Program pokazuje uzupełnienie danymi listy
# o ustalonej długości.

# Program prosi użytkownika o podanie
# temperatur powietrza odnotowanych przez
# 7 kolejnych dni.

#Stała NUM_DAYS określa liczbę dni, dla których
#będą pobierane dane od użytkownika.

NUM_DAYS = 7

def main():
    #utworzenie listy o ustalonej liczbie
    #elementów - wszytskie równe zero
    temperatures = [0]*NUM_DAYS

    #pobranie od użytkownika temperatur z kolejnych dni
    print('Podaj temperatury odnotowane przez 7 dni.')
    index = 0
    while index < NUM_DAYS:
        print('Dzień nr ',index+1,': ',sep='',end='')
        temperatures[index] = float(input())
        index += 1

    #wyświetlenie pobranych danych
    print('Odnotowane temperatury')
    for t in temperatures:
        print(t)

main()












