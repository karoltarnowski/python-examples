# Program pokazuje odczyt listy z pliku.
# Program wykorzystuje metodę readlines obiektu plikowego.

def main():
    infile = open('lista.txt','r')
    #Funkcja readlines() odczytuje z pliku wiersze
    #i zapisuje je w liście
    courses = infile.readlines()
    infile.close()

    print(courses)

main()














