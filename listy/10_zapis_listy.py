# Program pokazuje zapisywanie elementów listy do pliku.
# Program wykorzystuje metodę writelines obiektu plikowego.

def main():
    courses = ['Analiza\n', 'Algebra\n', 'Fizyka\n']

    outfile = open('lista.txt','w')
    #funkcja writlines() zapisuje elementy listy w pliku
    outfile.writelines(courses)
    outfile.close()

main()












