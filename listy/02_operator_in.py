# Program pokazuje sprawdzanie czy podany
# element znajduje się na liście.
# Program pokazuje użycie operatora in.

def main():
    #utworzenie listy kolorów podstawowych
    primary_colours = ['czerwony', 'zielony', 'niebieski']

    #pobranie nazwy koloru od użytkownika
    colour = input('Podaj nazwę koloru: ')

    #sprawdzenie, czy podany kolor jest na liście
    if colour in primary_colours:
        print('Kolor',colour,'jest kolorem podstawowym.')
    else:
        print('Kolor',colour,'nie jest kolorem podstawowym.')
        
    
main()












