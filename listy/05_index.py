# Program pokazuje użycie metody index()
# do odnalezienia pozycji elementu na liście.
# Przed dodaniem koloru do listy sprawdza, czy
# podany kolor się nie powtarza. Jeśli się powtarza,
# to sprawdza, na której pozycji jest powtórzenie.

def main():
    #utworzenie pustej listy
    colours = []

    #zmienna kontroluje przebieg pętli while
    keep_going = 't'
    while keep_going == 't':
        #pobranie nazwy koloru od użytkownika
        colour = input('Podaj kolor: ')

        #jeśli podanej nazwy nie ma liście
        if colour not in colours:
            #to ją dołącz
            colours.append(colour)
        else:
            #w p. p. wyświetl komunikat i przerwij pętle
            print('Kolor ',colour,' jest już na liście. ',
                  'Na pozycji ',colours.index(colour),'.',sep='')
            keep_going = 'n'

    #wypisanie wszystkich kolorów
    print('Na liście są następujące kolory:')
    for colour in colours:
        print(colour)
        
    
main()
















