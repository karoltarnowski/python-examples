# Program pokazuje przetwarzanie listy.
# Program oblicza średnią liczb z listy.

def main():
    #lista z przykładowymi danymi
    data = [1,2,3,5,8,13]
    #wywołanie funkcji average()
    #lista data przekazana jako argument 
    print('Średnia danych z listy to',average(data))

#I: lista zawierająca dane do obliczeń
#P: funkcja oblicza sumę liczb na liście,
    # a następnie zwraca średnią
    # (iloraz sumy elementów i liczby elementów)
#O: średnia elementów listy
def average(data):
    total = 0
    for value in data:
        total += value
    return total / len(data)

main()



