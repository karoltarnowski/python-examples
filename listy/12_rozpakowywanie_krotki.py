# Program pokazuje wykorzystanie pakowania i rozpakowywania krotki
# do zwracania kilku wartości przez funkcję.

def main():
    # przypisanie dwóch wartości
    # zwracanych przez funkcję
    # do zmiennych x0, y0
    x0, y0 = fun(5, 8)
    print(x0, y0)

    # przypisanie dwóch wartości zapakowanych
    # w krotkę do zmiennej k
    k = fun(5, 8)
    print(type(k)) # <class 'tuple'>
    print(k)       # wyświetlenie krotki

    x1, y1 = k     # rozpakowanie krotki do zmiennych x1, y1
    print(x1, y1)
    

# funkcja przyjmuje dwa argumenty
# oblicza ich sumę oraz różnicę
# wartości zwraca w dwuelementowej krotce
def fun(a, b):
    s = a + b
    r = a - b
    return (s, r)

main()

 

















