# Program pokazuje przetwarzanie listy.
# Program oblicza średnią liczb z listy.

def main():
    #lista z przykładowymi danymi
    data = [1,2,3,5,8,13]

    #obliczenie sumy elementów z listy
    total = 0
    for value in data:
        total += value
    #obliczenie średniej elementów
    average = total / len(data)
    #wypisanie wyniku
    print('Średnia danych z listy to',average)

main()












