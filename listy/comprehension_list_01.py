# -*- coding: utf-8 -*-
"""
Przykład użycia wyrażenia listowego
(comprehension list, listy składanej)
do utworzenia listy.
"""

square = []
for x in range(5):
    square += [x*x]
print(square)

square2 = [x*x for x in range(5)]
print(square2)

square3 = (x*x for x in range(5))
print(square3)


