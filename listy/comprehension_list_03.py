# -*- coding: utf-8 -*-
"""
Przykłady użycia wyrażenia listowego
(comprehension list, listy składanej).

Przykład pochodzi ze strony internetowej:
    https://www.w3schools.com/python/python_lists_comprehension.asp
"""

fruits = ["apple", "banana", "cherry", "kiwi", "mango"]

newlist = [x for x in fruits if "a" in x]
print(newlist)
input()

newlist = [x for x in fruits if x != "apple"]
print(newlist)
input()

newlist = [x for x in fruits]
print(newlist)
input()

newlist = [x.upper() for x in fruits]
print(newlist)
input()

newlist = ['hello' for x in fruits]
print(newlist)
input()

newlist = [x if x != "banana" else "orange" for x in fruits]
print(newlist)
input()













