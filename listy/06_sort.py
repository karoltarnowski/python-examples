# Program pokazuje użycie metody sort()
# do uporządkowania listy elementów.

def main():
    #utworzenie pustej listy
    colours = []

    #zmienna kontroluje przebieg pętli while
    keep_going = 't'
    while keep_going == 't':
        #pobranie nazwy koloru od użytkownika
        colour = input('Podaj kolor: ')

        #jeśli podanej nazwy nie ma liście
        if colour not in colours:
            #to ją dołącz
            colours.append(colour)
        else:
            #w p. p. wyświetl komunikat i przerwij pętle
            print('Kolor ',colour,' jest już na liście. ',
                  'Na pozycji ',colours.index(colour),'.',sep='')
            keep_going = 'n'

    #sortowanie listy w kolejności alfabetycznej
    colours.sort()
    
    #wypisanie wszystkich kolorów
    print('Na liście są następujące kolory:')
    for colour in colours:
        print(colour)
        
    
main()
















