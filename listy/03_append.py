# Program pokazuje użycie metody append()
# do dołączania elementów do listy.

# Program prosi użytkownika o wymienianie
# nazw kolorów jakie zna.

def main():
    #utworzenie pustej listy
    colours = []

    #zmienna kontroluje przebieg pętli while
    keep_going = 't'
    while keep_going == 't':
        #pobranie nazwy koloru od użytkownika
        colour = input('Podaj kolor: ')

        #dodanie nazwy do listy
        colours.append(colour)

        #pytanie czy kontynouwać wykonywanie pętli
        print('Czy znasz jeszcze jakieś kolory?')
        keep_going = input('Jeśli tak, wpisz t,'+
                           ' w przeciwnym razie inny znak: ')

    #wypisanie wszystkich kolorów
    print('Wszystkie kolory jakie podałeś:')
    for colour in colours:
        print(colour)
        
    
main()












