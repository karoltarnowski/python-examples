# -*- coding: utf-8 -*-
"""
Przykład pokazujący wykorzystanie domknięcia.
"""

# funkcja multiplication przyjmuje mnożnik
# zwraca funkcję, która przyjmuje mnożną
# i oblicza wynik mnożenia
def multiplication(multiplier):
    def multipy(multiplicant):
        return multiplicant*multiplier
    
    return multipy

# funkcja multiplication dla mnożnika 2 zwraca
# funkcję podwającą
doubler = multiplication(2)
print(doubler(4))

# funkcja multiplication dla mnożnika 2 zwraca
# funkcję potrajającą
tripler = multiplication(3)
print(tripler(7))


