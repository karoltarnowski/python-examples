# -*- coding: utf-8 -*-
"""
Przykład pokazujący tworzenie obiektu Figure.
"""

#import matplotlib as mpl
import matplotlib.pyplot as plt
#import numpy as np

# utworzenie obiektu wykresu (Figure)
# tworzenie pustego wykresu bez osi
fig = plt.figure() 
# tworzenie wykresu z jednym układem współrzędnych
fig, ax = plt.subplots() 
# tworzenie wykresu z siatką 2x2 układów współrzędnych
fig, axs = plt.subplots(2, 2)


