# -*- coding: utf-8 -*-
"""
Przykład pokazujący różne sposoby
definiowania kolorów.
"""

#import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

# przygotowanie danych wejciowych
x1, y1, x2, y2 = np.random.rand(4, 25)

# utworzenie wykresu
fig, ax = plt.subplots(figsize=(5, 2.7))
# kolor wypełnienia znaczników - biały
# kolor krawędzi znaczników - czerwony 
ax.scatter(x1, y1, s=50, facecolor="white", edgecolor="r")
# kolor wypełnienia znaczników - niebieski
# kolor krawędzi znaczników - czarny
ax.scatter(x2, y2, s=100, facecolor="C0", edgecolor=(0., 0., 0.))
# więcej o nazywaniu kolorów
# https://matplotlib.org/stable/tutorials/colors/colors.html






