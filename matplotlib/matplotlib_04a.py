# -*- coding: utf-8 -*-
"""
Przykład pokazujący podejście obiektowe i proceduralne.
"""

#import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

# przygotowanie danych
x = np.linspace(-10,10,100)
y1 = 5*np.sin(x)/x
y2 = x*np.cos(x)

# stworzenie wykresu (podejście obiektowe)
# fig - obiekt wykresu
# ax - obiekt układu współrzędnych
fig, ax = plt.subplots()
# wywołanie metody plot() obiektu ax
ax.plot(x, y1)
ax.plot(x, y2)


