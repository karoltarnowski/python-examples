# -*- coding: utf-8 -*-
"""
Przykład pokazujący możlwość wprowadzania
oznaczeń tekstowych na wykresie.

"""

#import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

# przygotowanie dwóch zestawów danych losowych
mu1, sigma1 = 65, 20
x1 = mu1 + sigma1 * np.random.randn(10000)
mu2, sigma2 = 100, 30
x2 = mu2 + sigma2 * np.random.randn(10000)

# utworzenie wykresu
fig, ax = plt.subplots(figsize=(5, 2.7))
# zawierającego dwa histogramy
ax.hist(x1, 50, alpha=0.75) # kanał alpha - przezroczystość
ax.hist(x2, 50, alpha=0.75)
# dobranie zakresów osi
ax.axis([-25, 200, 0, 700])
# umieszczenie opisów histogramów
ax.text(-15, 620, r"$\mu_1=65,\ \sigma_1=20$")
ax.text(125, 620, r"$\mu_2=100,\ \sigma_2=30$")
# dodanie siatki na wykresie
ax.grid(True)













