# -*- coding: utf-8 -*-
"""
Przykład pokazujący podejście obiektowe i proceduralne.
"""

#import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

# przygotowanie danych
x = np.linspace(-10,10,100)
y1 = 5*np.sin(x)/x
y2 = x*np.cos(x)

# stworzenie wykresu (podejście proceduralne)
# wywołanie funkcji figure() tworzącej wykres
plt.figure()
# wywołanie funkcji plot() tworzącej obiekt linii
# w osiach wykresu
plt.plot(x, y1)
plt.plot(x, y2)






