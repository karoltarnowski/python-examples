# -*- coding: utf-8 -*-
"""
Przykład pokazujący wykres typu scatter.
Dane do wykresu przekazane jako słownik.
Punkty mają różne rozmiary oraz kolory.
"""

#import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

# przygotowanie danych w postaci słownika
# zawierającego tablice ndarray
data = {'x': np.arange(50) }
data['y'] = data['x'] + 10 * np.random.randn(50)
data['s'] = np.random.randint(20, 60, 50)
data['c'] = np.random.randint(0, 50, 50)

# utworzenie wykresu
fig, ax = plt.subplots(figsize=(5, 2.7))
# argument c - pozwala przekazać kolor znacznika
# argument s - pozwala przekazać rozmiar znacznika
ax.scatter('x', 'y', c='c', s='s', data=data)
ax.set_xlabel('dane x')
ax.set_ylabel('dane y')





