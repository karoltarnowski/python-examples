# -*- coding: utf-8 -*-
"""
Przykład pokazujący wykorzystanie skali logarytmicznej.

"""

#import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

# przygotowanie danych losowych
mu1, sigma1 = 1, 1
y = mu1 + sigma1 * np.random.randn(50)
z = 10**y
x = np.arange(len(y))

# utworzenie wykresu o dwóch układach współrzędnych
fig, axs = plt.subplots(2, 1, figsize=(5, 5))
# wykres w skali liniowej
axs[0].plot(x,z)

# wykres w skali logarytmicznej
axs[1].plot(x,z)
axs[1].set_yscale('log')




















