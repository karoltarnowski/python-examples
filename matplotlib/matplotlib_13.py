# -*- coding: utf-8 -*-
"""
Przykład pokazujący umieszczanie dodatkowych osi
w układzie współrzędnych.

"""

#import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

t = np.linspace(0, 6*np.pi, 101)
s = np.sin(t)

fig, (ax1, ax3) = plt.subplots(1, 
                               2, 
                               figsize=(7, 2.7), 
                               constrained_layout=True)
l1, = ax1.plot(t, s)
ax2 = ax1.twinx()
l2, = ax2.plot(t, t, 'C1')
ax2.legend([l1, l2], ['sin(t) (lewa oś)', 't (prawa oś)'])

ax3.plot(t, s)
ax3.set_xlabel('faza [rad]')
ax4 = ax3.secondary_xaxis('top', 
                          functions=(np.rad2deg, np.deg2rad))
ax4.set_xlabel('faza [°]')



















