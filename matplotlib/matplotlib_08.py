# -*- coding: utf-8 -*-
"""
Przykład pokazujący wykorzystanie funkcji pomocniczej
do tworzenia wykresu.
"""

#import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

def plotter(ax, data_x, data_y, params):
    """
    Funkcja pomocnicza do tworzenia wykresów.
    """
    out = ax.plot(data_x, data_y, **params)
    return out

# przygotowanie danych wejściowych
phi = np.linspace(0,2*np.pi)
x1 = np.cos(phi)
y1 = np.sin(phi)
x2 = np.cos(3*phi)
y2 = np.sin(2*phi)

# utworzenie wykresu
fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(5, 2.7))
plotter(ax1, x1, y1, {"marker": "+"})
plotter(ax2, x2, y2, {"marker": "^"})



















