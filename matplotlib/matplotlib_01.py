# -*- coding: utf-8 -*-
"""
Przykład pokazujący tworzenie prostego wykresu.
"""

#import matplotlib as mpl
import matplotlib.pyplot as plt
#import numpy as np

#utworzenie obiektu wykresu (figure)
fig, ax = plt.subplots() 
x = [1, 2, 3, 4, 5]
y = [5, 1, 4, 2, 3]
ax.plot(x, y)


