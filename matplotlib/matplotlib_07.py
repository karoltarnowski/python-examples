# -*- coding: utf-8 -*-
"""
Przykład pokazujący wykres zawierający trzy
serie danych oraz legendę.
"""

#import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

# przygotowanie wektora argumentów (odciętych)
x = np.linspace(-1,1)

# utworzenie wykresu
fig, ax = plt.subplots(figsize=(5, 2.7))
# dodanie funkcji liniowej do wykresu
ax.plot(x, x, label="liniowa")
# dodanie funkcji kwadratowej
ax.plot(x, x**2, label="kwadratowa")
# dodanie funkcji sześciennej
ax.plot(x, x**3, label="sześcienna")

ax.set_title('wykres trzech funkcji')
ax.set_xlabel('odcięta')
ax.set_ylabel('rzędna')
# umieszczenie legendy
ax.legend()




