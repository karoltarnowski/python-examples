# -*- coding: utf-8 -*-
"""
Przykład pokazujący różne sposoby
modyfikowania właściwości serii danych.
"""

#import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

# przygotowanie danych wejciowych
phi = np.linspace(0,2*np.pi,21)
x = np.sin(phi)
y = np.cumsum(x)

# utworzenie wykresu
fig, ax = plt.subplots(figsize=(5, 2.7))
# wykres pierwszej serii danych 
# ustalone kolor, styl i szerokość linii
ax.plot(phi, x, color="red", linestyle="--", linewidth=2)
# wykres drugiej serii danych 
l2, = ax.plot(phi, y)
# ustalone kolor, styl i szerokość linii
l2.set_color("blue")
l2.set_linestyle(":")
l2.set_linewidth(3)


















