# -*- coding: utf-8 -*-
"""
Przykład pokazujący tworzenie wykresów trójwymiarowych.

"""

#import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

# dane do wykkresów
x = np.linspace(-10, 10, 100) # współrzędne x
y = np.linspace(-10, 10, 100) # współrzędne y
X, Y = np.meshgrid(x, y) # siatka współrzędnych X, Y
R = (X**2 + Y**2)**0.5   # obliczone R
Z = np.sin(R)/R          # sinc(r)

# przykładowe wykresy 2d
fig, (ax1, ax2) = plt.subplots(1, 
                               2, 
                               figsize=(7, 2.7), 
                               constrained_layout=True)

pc = ax1.pcolormesh(X, Y, Z, vmin=-1, vmax=1)
fig.colorbar(pc, ax=ax1)
ax1.set_title('pcolormesh')

co = ax2.contourf(X, Y, Z, levels=np.linspace(-1,1,5))
fig.colorbar(co, ax=ax2)
ax2.set_title('contourf')

# przykładowy wykres 3d
fig, ax3 = plt.subplots(1, subplot_kw = {'projection': '3d'})
ax3.plot_surface(X,Y,Z)
ax3.set_title('plot_surface')




















