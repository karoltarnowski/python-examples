# -*- coding: utf-8 -*-
"""
Prosty przykład ilustrujący tworzenie hierarchii klas.
"""

# definicja klasy bazowej reprezentującej zwierzę
class Animal:
    # klasa implementuje konstruktor
    # jedynym atrybutem obiektu jest m (masa)
    def __init__(self, m):
        self.m    = m
    
    # klasa implemenutuje metodę moves(),
    # która wyświetla ogólną informację o zwierzętach
    def moves(self):
        print("Zwierzęta się przemieszczają.")
        
# definicja klasy pochodnej
class Bird(Animal):
    # klasa pochodna ma wszystkie cechy klasy bazowej,
    # a dodatkowo implementuje metodę flights(),
    # która wyświetla informację o ptakach
    def flights(self):
        print("Ptaki latają.")

# definicja klasy pochodnej
class Mammal(Animal):
    # klasa pochodna implementuje metodę runs(),
    # która wyświetla informację o ssakach
    def runs(self):
        print("Ssaki biegają.")

# utworzenie obiektu klasy Animal
a = Animal(1.)
print(a.m)
a.moves()

# klasa Bird dziedziczy konstruktor po klasie Animal
b = Bird(0.2)
print(b.m)
# obiekt tej klasy ma dodatkową metodę flights()
b.flights()

# klasa Mammal dziedziczy konstruktor po klasie Animal
m = Mammal(1000)
print(m.m)
# obiekt tej klasy ma dodatkową metodę runs()
m.runs()








