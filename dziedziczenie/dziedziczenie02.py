# -*- coding: utf-8 -*-
"""
Prosty przykład ilustrujący tworzenie hierarchii klas.
Klasy pochodne przeciążają metodę klasy bazowej.
"""

# definicja klasy bazowej reprezentującej zwierzę
class Animal:
    def __init__(self, m):
        self.m    = m
    
    # klasa implemenutuje metodę moves(),
    # która będzie przeciążana przez klasy pochodne
    def moves(self):
        print("Zwierzęta się przemieszczają.")
        
class Bird(Animal):
    # klasa Bird przeciąża metodę moves()
    # pochodzącą z klasy bazowej
    def moves(self):
        print("Ptaki latają.")

class Mammal(Animal):
    # klasa Mammal również przeciąża metodę moves()
    # pochodzącą z klasy bazowej
    def moves(self):
        print("Ssaki biegają.")
        
# klasa Reptiles nie przeciąża metody moves()
class Reptiles(Animal):
    pass

a = Animal(1.)
b = Bird(0.2)
m = Mammal(1000)
r = Reptiles(300)
# obiekty różnych klas zebranej na wspólnej liście
animals = [a, b, m, r]

# wywołanie metody moves() uruchomi różne wersje tej metody
# w zależności od klasy obiektu
for z in animals:
    print(z.m)
    z.moves()
    
    
    
    
    
    
    
    
    
    
    
    
    