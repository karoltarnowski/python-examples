#Program sprawdza jakiego rodzaju są znaki
#ciągu tekstowego podanego przez użytkownika.
#Program ilustruje działanie metod:
# isalnum(), isdigit(), isalpha(), isspace(), islower(), isupper().

def main():
    text = input('Podaj ciąg tekstowy: ')

    if text.isalnum():
        print('Ciąg tekstowy jest alfanumeryczny.')
    if text.isdigit():
        print('Ciąg tekstowy zawiera jedynie cyfry.')
    if text.isalpha():
        print('Ciąg tekstowy zawiera jedynie litery.')
    if text.isspace():
        print('Ciąg tekstowy zawiera jedynie białe znaki.')
    if text.islower():
        print('Ciąg tekstowy zawiera jedynie małe litery.')
    if text.isupper():
        print('Ciąg tekstowy zawiera jedynie duże litery.')

if __name__ == '__main__':
    main()
