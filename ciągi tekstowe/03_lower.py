#Program ilustruje wykorzystanie metody lower()
#do sprawdzenia znaku podanego przez użytkownika.

def main():
    keep_going = 't'

    print('To jest program zadający pytanie.')
    while keep_going.lower() == 't':
        print('Czy mam powtórzyć?')
        keep_going = input("Jeśli tak, wpisz 't', w p.p. inny znak: ")

if __name__ == '__main__':
    main()
