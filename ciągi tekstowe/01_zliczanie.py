#Program zlicza wystąpienia litery 's'
#w podanym ciągu tekstowym.
#Program ilustruje wykorzystanie pętli for
#do uzyskania dostępu do znaków ciągu tekstowego.

def main():
    count = 0
    text = input('Podaj dowolne zdanie: ')

    for ch in text:
        if ch == 'S' or ch =='s':
            count += 1

    print('Liczba wystąpień litery S: ', count, end = '.\n')

if __name__ == '__main__':
    main()
