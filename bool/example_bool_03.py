#Program pokazuje sprawdzanie parzystość liczby.
#Do sprawdzania parzystości wykorzystywana jest
#funkcja is_even() zwracająca wartość logiczną.

def main():
    x = 14

    #wywołanie funkcji is_even() w warunku instrukcji if
    if is_even(x):
        print('Liczba', x, 'jest parzysta')
    else:
        print('Liczba', x, 'jest nieparzysta')

#I: liczba całkowita
#P: sprawdzane jest, czy reszta z dzielenia przez 2
#   równa się 0
#O: wartość logiczna:
#   True (jeśli liczba jest parzysta)
#   False (w p. p.)
def is_even(x):
    return x % 2 == 0

main()




