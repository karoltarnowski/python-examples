#Program pokazuje działanie operatorów logicznych.

def main():
    x = int(input("Podaj liczbę całkowitą: "))

    if x > 20 and x < 40:
        print("Liczba mieści się w przedziale (20,40).")

    if x <= 20 or x >= 40:
        print("Liczba nie mieści się w przedziale (20,40).")

    if x != 30:
        print("Liczba jest różna od 30.")

    if not x == 30:
        print("Liczba jest różna od 30.")


main()
