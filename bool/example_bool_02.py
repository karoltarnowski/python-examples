#Program pokazuje sprawdzanie parzystość liczby.
#Wynik sprawdzenia (wartość logiczna)
#zapisywany jest w zmiennej even.

def main():
    x = 14

    # Zmienna even zapamiętuje, czy liczba jest parzysta.
    even = x % 2 == 0

    if even:
        #jest parzysta
        print('Liczba', x, 'jest parzysta')
    else:
        #w p. p. jest nieparzysta
        print('Liczba', x, 'jest nieparzysta')

    # Drugi raz wykorzystujemy obliczoną zmienną.
    if even:
        print('Ciągle parzysta')
        
main()




