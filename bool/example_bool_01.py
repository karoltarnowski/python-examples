#Program pokazuje sprawdzanie parzystość liczby.
#Parzystość liczby jest związana z podzielnością przez 2.

def main():
    x = 14

    # Jeśli liczba dzieli się przez 2 bez reszty
    # (reszta z dzielenia przez 2 równa się 0),
    if x % 2 == 0:
        #to liczba jest parzysta
        print('Liczba', x, 'jest parzysta')
    else:
        #w p. p. jest nieparzysta
        print('Liczba', x, 'jest nieparzysta')
        
main()




