#Moduł 04_sort zawiera funkcję swap(), która pozwala zamienić
#miejscami dwa elementy na liście.

#Moduł zawiera również funkcję main(), która demonstruje działanie
#funkcji swap().

def swap(l,left,right):
    # item     = l[left]
    # l[left]  = l[right]
    # l[right] = item
    l[left], l[right] = l[right], l[left]

def main():
    l = list(range(4))
    print(l)
    swap(l,1,2)
    print(l)

#Instrukcja warunkowa, która sprawdza, czy plik był
#uruchomiony jako skrypt, czy załadowany jako moduł.
if __name__ == "__main__":
    main()
