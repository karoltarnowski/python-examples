#Moduł sort.py zawiera funkcje implementujące
#różne algorytmy sortowania i funkcje pomocnicze.

import random #wykorzystywana funkcja shuffle()

def bubblesort(l):
    n = len(l)
    for i in range(n-1):
        for j in range(n-i-1):
            if l[j+1] < l[j]:
                swap(l,j,j+1)
  
def swap(l,left,right):
    item     = l[left]
    l[left]  = l[right]
    l[right] = item

def main():
    l = list(range(13))
    random.shuffle(l)    
    print(l)
    bubblesort(l)
    print(l)
    
#Instrukcja warunkowa, która sprawdza, czy plik był
#uruchomiony jako skrypt, czy załadowany jako moduł.
if __name__ == "__main__":
    main()
