#Program przedstawia funkcję my_max(),
#która znajduje największy element na liście.
#Wykorzystuje indeks do przejścia przez elementy listy.

#Lista jest pomieszana funkcją shuffle z modułu random.

import random #import modułu - wykorzystywana funkcja shuffle

def my_max(l):
    m = l[0]
    n = len(l)
    for i in range(1,n):
        if l[i] > m:
            m = l[i]
        
    return m
            
def main():
    #utworzenie listy zawierającej liczby całkowite
    l = list(range(8))
    #pomieszanie kolejności liczb
    random.shuffle(l)
    print('Zawartość pomieszanej listy: ',l)
    print('Element maksymalny znaleziony funkcją my_max():', my_max(l))

main()
    
