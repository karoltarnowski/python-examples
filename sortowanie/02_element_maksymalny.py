#Program przedstawia funkcję my_max(),
#która znajduje największy element na liście.

#Lista jest pomieszana funkcją shuffle z modułu random.

import random #import modułu - wykorzystywana funkcja shuffle

def my_max(l):
    m = l[0]
    for item in l[1:]:
        if item > m:
            m = item
    return m
            
def main():
    #utworzenie listy zawierającej liczby całkowite
    l = list(range(8))
    #pomieszanie kolejności liczb
    random.shuffle(l)
    print('Zawartość pomieszanej listy: ',l)
    print('Element maksymalny znaleziony funkcją my_max():', my_max(l))

main()
    
