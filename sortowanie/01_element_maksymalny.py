#Program ilustruje działanie funkcji wbudowanej max()
#do znalezienia największego elementu na liście.

#Lista jest pomieszana funkcją shuffle z modułu random.

import random #import modułu - wykorzystywana funkcja shuffle

def main():
    #utworzenie listy zawierającej liczby całkowite
    l = list(range(8))
    #pomieszanie kolejności liczb
    random.shuffle(l)
    print('Zawartość pomieszanej listy: ',l)
    print('Element maksymalny znaleziony funkcją max():', max(l))

main()
    
