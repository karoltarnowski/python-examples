"""
Przykład pokazujący definicję klasy
zawierającą konstruktor.
"""

# definicja klasy Point2d
class Point2d:

    # definicja konstruktora
    def __init__(self):
        self.x = 0.
        self.y = 0.

def main():
    # utworzenie instancji klasy
    my_point = Point2d()
    # bezpośredni odczyt pól klasy
    print('współrzędne punktu: [', 
          my_point.x, ', ', 
          my_point.y, ']', sep = '')

if __name__ == '__main__':
    main()














