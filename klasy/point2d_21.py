"""
Program ilustrujący działanie klasy z przeciążonym
operatorem dodwania.
"""
class Point2d_Operators:

    def __init__(self, x = 0., y = 0.):
        self.__x = x
        self.__y = y

    def get_coordinates(self):
        return [ self.__x,  self.__y ]
    
    def set_xy(self,x,y):
        self.__x = x
        self.__y = y
        
    # definicja metody __add__, która jest wywoływana
    # przy obliczaniu wyrażenia self + other
    def __add__(self, other):
        print("__add__")
        r = Point2d_Operators(self.__x + other.__x, self.__y + other.__y)
        return r
        
    def __str__(self):
        return str(self.get_coordinates())

def main():
    # zmienna p1 - punkt o współrzędnych [3., 4.]
    p1 = Point2d_Operators()
    p1.set_xy(3.,4.)
    
    # zmienna p2 - punkt o współrzędnych [2., 0.7]
    p2 = Point2d_Operators()
    p2.set_xy(2.,0.7)
    
    print( p1 + p2 )

if __name__ == '__main__':
    main()



















