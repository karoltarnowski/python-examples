"""
Przykład pokazujący użycie pól prywatnych.
"""
import random

class Point2d:

    def __init__(self):
        #inicjalizacja pól prywatnych
        self.__x = 0.
        self.__y = 0.

    def random(self):
        self.__x = random.uniform(0,1)
        self.__y = random.uniform(0,1)

    def get_coordinates(self):
        return [self.__x, self.__y]

def main():
    # dane w klasie obsługiwane
    # są tylko za pomocą metod
    my_point = Point2d()
    print('współrzędne punktu: ',
          my_point.get_coordinates(), sep = '')
    my_point.random()
    print('współrzędne punktu: ',
          my_point.get_coordinates(), sep = '')
    # poniższa instrukcja powoduje błąd
    # print(my_point.__x)
        

if __name__ == '__main__':
    main()

















