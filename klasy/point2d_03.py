import random

class Point2d:

    def __init__(self):
        self.x = 0.
        self.y = 0.

    def random(self):
        self.x = random.uniform(0,1)
        self.y = random.uniform(0,1)        

def main():
    my_point = Point2d()
    print('współrzędne punktu: [', my_point.x, ', ', my_point.y, ']', sep = '')
    my_point.random()
    my_point.x = .5
    my_point.y = .5
    print('współrzędne punktu: [', my_point.x, ', ', my_point.y, ']', sep = '')
        

if __name__ == '__main__':
    main()
