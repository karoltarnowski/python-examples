"""
Przykład pokazujący użycie pól prywatnych.
"""
import random

class Point2d:

    def __init__(self):
        self.__x = 0.
        self.__y = 0.

    def random(self):
        self.__x = random.uniform(0,1)
        self.__y = random.uniform(0,1)

    def get_coordinates(self):
        return [self.__x, self.__y]

def main():
    my_point = Point2d()
    print('współrzędne punktu: ',
          my_point.get_coordinates(), sep = '')
    my_point.random()
    # poniższe instrukcje nie dają dostępu do
    # prywatnych pól klasy
    my_point.__x = .5 
    my_point.__y = .5
    print('współrzędne punktu: ',
          my_point.get_coordinates(), sep = '')
        

if __name__ == '__main__':
    main()
















