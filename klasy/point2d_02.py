"""
Przykład pokazujący definicję klasy
zawierającą konstruktor i metodę
"""
import random

class Point2d:

    def __init__(self):
        self.x = 0.
        self.y = 0.

    #definicja metody random()
    def random(self):
        """
        Meotda nadaje współrzędnym punktu
        losowe wartości

        """
        self.x = random.uniform(0,1)
        self.y = random.uniform(0,1)        

def main():
    my_point = Point2d()
    print('współrzędne punktu: [',
          my_point.x, ', ', my_point.y, ']', sep = '')
    #wywołanie metody
    my_point.random()
    print('współrzędne punktu: [',
          my_point.x, ', ', my_point.y, ']', sep = '')
        

if __name__ == '__main__':
    main()




