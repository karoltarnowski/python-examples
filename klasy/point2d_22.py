"""
Program ilustrujący działanie klasy z przeciążonym
operatorem dodawania.
"""
class Point2d_Operators:

    def __init__(self):
        self.__x = 0.
        self.__y = 0.

    def get_coordinates(self):
        return [ self.__x,  self.__y ]
    
    def set_xy(self,x,y):
        self.__x = x
        self.__y = y
        
    # definicja metody __add__, która jest wywoływana
    # przy obliczaniu wyrażenia self + other
    def __add__(self, other):
        print("__add__")
        if type(other) == type(self):
            r = Point2d_Operators()
            r.set_xy(self.__x + other.__x, self.__y + other.__y)
            return r
        elif type(other) == int or type(other) == float:
            t = Point2d_Operators()
            t.set_xy(float(other), 0.0)
            return self + t
        
    def __str__(self):
        return str(self.get_coordinates())

def main():
    # zmienna p1 - punkt o współrzędnych [3., 4.]
    p1 = Point2d_Operators()
    p1.set_xy(3.,4.)
    
    # zmienna p2 - punkt o współrzędnych [2., 0.7]
    p2 = Point2d_Operators()
    p2.set_xy(2.,0.7)
    
    print( p1 + p2 )
    print( p1 + 3. )
    print( p1 + 3 )

    print( 3. + p1 )

if __name__ == '__main__':
    main()



















