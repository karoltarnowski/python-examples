"""
Przykład pokazujący definicję klasy
zawierającą metodę dającą dostęp do danych w klasie.
"""
import random

class Point2d:

    def __init__(self):
        self.x = 0.
        self.y = 0.

    def random(self):
        self.x = random.uniform(0,1)
        self.y = random.uniform(0,1)

    #definicja metody
    def get_coordinates(self):
        return [self.x, self.y]

def main():
    my_point = Point2d()
    #wywołanie metody get_coordinates()
    print('współrzędne punktu: ',
          my_point.get_coordinates(), sep = '')
    my_point.random()
    print('współrzędne punktu: ',
          my_point.get_coordinates(), sep = '')
        

if __name__ == '__main__':
    main()


















