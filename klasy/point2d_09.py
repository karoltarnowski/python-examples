"""
Program wykorzystuje klasę zaimplementowaną
w osobnym module.
"""
import point2d

def main():
    my_point = point2d.Point2d()
    # obiekt klasy jako argument funkcji print
    # do wyświetlenia wykorzystywana jest metoda __str__()
    print('współrzędne punktu: ', my_point)
    my_point.set_xy(.5, .5)
    print('współrzędne punktu: ', my_point)
        

if __name__ == '__main__':
    main()



















