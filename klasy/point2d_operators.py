"""
Implementacja klasy z przeciążonym operatorem dodawania.
"""
import random

class Point2d_Operators:

    def __init__(self):
        self.__x = 0.
        self.__y = 0.

    def random(self):
        self.set_xy(random.uniform(0,1), random.uniform(0,1))

    def get_coordinates(self):
        return [ self.__x,  self.__y ]
    
    def set_xy(self,x,y):
        self.__x = x
        self.__y = y
        
    def __add__(self, other):
        if type(other) == Point2d_Operators:
            r = Point2d_Operators()
            r.set_xy(self.__x + other.__x, self.__y + other.__y)
            return r
        elif type(other) == float or type(other) == int:
            o = Point2d_Operators()
            o.set_xy(float(other) , 0.0)
            return self + o
        else:
            raise TypeError
            
    def __radd__(self,other):
        return self + other
        
    def __iadd__(self,other):
        print("iadd")
        self.__x += other.__x
        self.__y += other.__y
        return self
        
    def __str__(self):
        return str(self.get_coordinates())











