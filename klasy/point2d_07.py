"""
Przykład pokazujący wykorzystanie metody
nadającej wartości pól klasy
"""
import random

class Point2d:

    def __init__(self):
        self.__x = 0.
        self.__y = 0.

    def random(self):
        self.__x = random.uniform(0,1)
        self.__y = random.uniform(0,1)

    def get_coordinates(self):
        return [self.__x, self.__y]

    #definicja metody
    def set_xy(self,x,y):
        self.__x = x
        self.__y = y

def main():
    my_point = Point2d()
    print('współrzędne punktu: ',
          my_point.get_coordinates(), sep = '')
    #wywołanie metody
    my_point.set_xy(.5,.5)
    print('współrzędne punktu: ',
          my_point.get_coordinates(), sep = '')
        

if __name__ == '__main__':
    main()
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
