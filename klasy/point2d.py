"""
Implementacja klasy (wykorzystywana w programie point2d_09.py).
"""
import random

class Point2d:

    def __init__(self):
        self.__x = 0.
        self.__y = 0.

    def random(self):
        self.set_xy(random.uniform(0,1), random.uniform(0,1))

    def get_coordinates(self):
        return [ self.__x,  self.__y ]
    
    def set_xy(self,x,y):
        self.__x = x
        self.__y = y
        
    #funkcja implementuje konwersję obiektu na string
    def __str__(self):
        return str(self.get_coordinates())











