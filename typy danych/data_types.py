# -*- coding: utf-8 -*-
"""
Program pokazuje dynamiczną kontrolę typów.
"""

def main():
    # typy liczbowe
    show_data_type(1)
    show_data_type(1.0)
    show_data_type(1j)
    
    # inne typy proste
    show_data_type(True)
    show_data_type("napis")
    show_data_type(None)
    
    # typy złożone
    show_data_type([1, 2])
    show_data_type((1.0, 0.0))
    show_data_type({'k': 'v'})
    show_data_type({'k'})
    

# funkcja wyświetla zawartość argumentu
# oraz wyświetla informacje o jego typie
# wywołanie funkcji type()
def show_data_type(x):
    print(x,"jest typu",type(x))    
   
   
if __name__ == "__main__":
    main()
