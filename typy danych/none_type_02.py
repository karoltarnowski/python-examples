# -*- coding: utf-8 -*-
"""
Przykład pokazuje wykorzystanie typu NoneType,
gdy funkcja nie zwraca żadnej wartości.
"""

def main():
    #wypisanie wartości zwracanej przez funkcję fun()
    print(fun())
    
# funkcja fun() nie przyjmuje żadnych argumentów
# nie zwraca także żadnej wartości (brak instrukcji return)
# wartością zwracaną przez funckję jest None (typu NoneType)    
def fun():
    print("Komunikat z funkcji fun()")
    return
   
if __name__ == "__main__":
    main()




