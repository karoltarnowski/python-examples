#Program pokazuje porównanie ciągów teksowych.

def main():
    name1 = 'Maria'
    name2 = 'Marek'

    # Operator porównania sprawdza, czy dwa ciągi znaków
    # są sobie równe - czy są takie same.
    # W zależności od wyniku porównania instrukcja warunkowa
    # wybierze jedną z dwóch ścieżek.
    if name1 == name2:
        print('Imiona są takie same')
    else:
        print('Imiona nie są takie same')

main()




