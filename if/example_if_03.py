#Program pokazuje użycie instrukcji warunkowej.
#Zakończenie bloku poleceń zależnych od warunku
#osiąga się odpowiednimi wcięciami.

#Program pobiera od użytkownika punktację (liczbę całkowitą)
#i wyświetla gratulacje, jeśli wynik jest większy
#lub równy 50 pkt.

def main():
    rate = int(input('Podaj swoją punktację [0-100]: '))

    if rate >= 50:
        print('Gratulacje!')
        print('Świetny rezultat!')

    print('Niezależnie od wyniku, dzięki za udział.')

main()




