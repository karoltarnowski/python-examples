#Program pokazuje użycie instrukcji warunkowej if-elif-else.

#Program pobiera od użytkownika punktację (liczbę całkowitą)
#
#Jeśli wynik jest większy lub równy 90 pkt.
#       wyświetla informację o świetnym wyniku,
#jeśli wynik jest mniejszy niż 90 pkt., ale większy równy 50
#       wyświetla motywujący komunikat,
#jeśli wynik jest mniejszy niż 50, ale większy nić 40 pkt.
#       wyświelta informację, że zabrakło niewiele,
#w przeciwnym przypadku
#       wyświetla motywujący komunikat.
#
def main():
    rate = int(input('Podaj swoją punktację [0-100]: '))

    if rate >= 90:
        print('Świetny wynik!')
    elif rate >= 50:   
        print('Możesz się jeszcze poprawić!')
    elif rate >= 40:
        print('Zabrakło niewiele')
    else:
        print('Dużo pracy przed Tobą!')

main()



















