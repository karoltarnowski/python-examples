#Program pokazuje użycie zagnieżdżonych instrukcji warunkowych.

#Program pobiera od użytkownika punktację (liczbę całkowitą)
#
#Jeśli wynik jest większy lub równy 50 pkt.
#   wyświetla gratulacje.
#   Dodatkowo, jeśli wynik jest większy lub równy 90 pkt.
#       wyświetla informację o świetnym wyniku.
#   w przeciwnym wypadku
#       wyświetla motywujący komunikat.
#w przeciwnym przypadku
#   informuje złym wyniku.
#   Dodatkowo, jeśli wynik jest większy lub równy 40 pkt.
#       wyświelta informację, że zabrakło niewiele.
#   w przeciwnym przypadku
#       wyświetla motywujący komunikat.
#
def main():
    rate = int(input('Podaj swoją punktację [0-100]: '))

    if rate >= 50:
        print('Gratulacje!')
        if rate >= 90:
            print('Świetny wynik!')
        else:
            print('Możesz się jeszcze poprawić!')
    else:
        print('Twój wynik nie jest dobry!')
        if rate >= 40:
            print('Zabrakło niewiele')
        else:
            print('Dużo pracy przed Tobą!')

main()



















