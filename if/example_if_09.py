#Program pokazuje porównanie ciągów teksowych.

def main():
    name1 = 'Maria'
    name2 = 'Marek'

    # Operator większości sprawdza, czy ciąg pierwszy jest
    # większy niż drugi (w porządku alfabetycznym).
    if name1 >  name2:
        print('Imię: ', name1, '' , sep = '"', end = '')
        print(' jest większe niż imię: ', name2, '.', sep = '"')

    #    M    a    r    i   a
    #   77   97  114  105  97
    
    #    M    a    r    e   k
    #   77   97  114  101 107

    # M==M a==a r==r  i>e 

main()




