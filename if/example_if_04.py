#Program pokazuje użycie instrukcji warunkowej
#podwójnego wyboru.

#Program pobiera od użytkownika punktację (liczbę całkowitą)
#
#Jeśli wynik jest większy lub równy 50 pkt.
#   wyświetla gratulacje,
#w przeciwnym przypadku
#   informuje złym wyniku.
#
#Następnie dziękuje za udział.
#
def main():
    rate = int(input('Podaj swoją punktację [0-100]: '))

    if rate >= 50:
        print('Gratulacje!')
        print('Świetny rezultat!')
    else:
        print('Twój wynik nie jest dobry!')
        print('Powodzenia następnym razem')

    print('Niezależnie od wyniku, dzięki za udział.')

main()




