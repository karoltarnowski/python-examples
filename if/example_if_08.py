#Program pokazuje porównanie ciągów teksowych.

def main():
    # Użytkownik podaje hasło
    password = input('Podaj hasło: ')

    # Instrukcja warunkowa sprawdza, czy hasło jest poprawne.
    if password == '3.1415':
        print('Hasło jest poprawne.')
    else:
        print('Podane hasło nie jest poprawne.')

main()



