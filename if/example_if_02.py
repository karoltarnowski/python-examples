#Program pokazuje użycie instrukcji warunkowej.
#W bloku instrukcji if znajduje się kilka poleceń.

#Program pobiera od użytkownika punktację (liczbę całkowitą)
#i wyświetla gratulacje, jeśli wynik jest większy
#lub równy 50 pkt.

def main():
    rate = int(input('Podaj swoją punktację [0-100]: '))
    if rate >= 50:
        print('Gratulacje!')
        print('Świetny rezultat!')

main()






